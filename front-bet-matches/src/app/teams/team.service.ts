import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Team} from './team.model';
import {environment} from '../../environments/environment';
import {catchError, map, tap} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({providedIn: 'root'})
export class TeamService {
  private url = environment.betMatchesUrl + '/teams';

  constructor(private http: HttpClient) {
  }

  fetchTeams() {
    return this.http
      .get<Team[]>(
        this.url
      ).pipe(
        catchError(error => {
          return throwError(error);
        })
      );
  }
}
