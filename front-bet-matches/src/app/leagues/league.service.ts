import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {League} from "./league.model";
import {catchError, map} from "rxjs/operators";
import {throwError} from "rxjs";

@Injectable({providedIn: 'root'})
export class LeagueService {
  private url = environment.betMatchesUrl + '/leagues';

  constructor(private http: HttpClient) {

  }

  getAllLeaguesOfUser(id: string) {
    const fullUrl = this.url + "/users/" + id;
    return this.http
      .get<League[]>(
        fullUrl
      ).pipe(
        map((leagues) => {
          return leagues
        }),
        catchError(err => {
          return throwError(err)
        })
      );
  }

  getLeague(id: number) {
    const fullUrl = this.url + "/" + id;

    return this.http
      .get<League>(
        fullUrl
      ).pipe(
        map((league) => {
          return league
        }),
        catchError(err => {
          return throwError(err)
        })
      );
  }

  addLeague(league: League) {
    return this.http
      .post<League>(
        this.url,
        league
      );
  }

  addUserToLeague(leagueId, userId) {
    const fullUrl = this.url + "/" + leagueId + "/users/" + userId;
    return this.http
      .post<League>(
        fullUrl,
        ''
      )
  }
}

