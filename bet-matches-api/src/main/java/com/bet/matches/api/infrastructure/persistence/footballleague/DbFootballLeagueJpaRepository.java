package com.bet.matches.api.infrastructure.persistence.footballleague;

import com.bet.matches.api.core.footballleague.FootballLeague;
import org.springframework.data.jpa.repository.JpaRepository;

interface DbFootballLeagueJpaRepository extends JpaRepository<FootballLeague, Integer> {

}
