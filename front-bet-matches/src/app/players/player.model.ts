import {Team} from '../teams/team.model';

export interface Player {
  id: number;
  id_rapid_api: number;
  first_name: string;
  last_name: string;
  team: Team;
  goals: number;
  position: string;
  nationality: string;
  age: number;
}
