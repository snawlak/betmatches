package com.bet.matches.authservice.core.common.betmatchesapi;

import com.bet.matches.authservice.core.registration.RegistrationRequestBody;
import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class UserMapper {

    public static User toBetMatchesApiUser(final RegistrationRequestBody registrationRequestBody,
                                           final UUID userId) {
        final String username = registrationRequestBody.getUsername();
        final String email = registrationRequestBody.getEmail();

        return User.builder()
                .id(userId)
                .username(username)
                .email(email)
                .build();
    }
}
