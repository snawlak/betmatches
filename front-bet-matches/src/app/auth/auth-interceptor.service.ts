import {Injectable} from "@angular/core";
import {HttpHandler, HttpHeaders, HttpInterceptor, HttpParams, HttpRequest} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {exhaustMap, take} from "rxjs/operators";
import {LoggedUser} from "./logged-user.model";
import {log} from "util";

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    return this.authService.loggedUser.pipe(
      take(1),
      exhaustMap(loggedUser => {
        if (loggedUser) {
          let modifiedRequest = AuthInterceptorService.betMatchesApiRequest(request, loggedUser);
          return next.handle(modifiedRequest);
        } else {
          let modifiedRequest = AuthInterceptorService.authRequest(request);
          return next.handle(modifiedRequest);
        }

      })
    );
  }

  private static authRequest(request: HttpRequest<any>) {
    return request.clone(
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
        })
      });
  }

  private static betMatchesApiRequest(request: HttpRequest<any>, loggedUser: LoggedUser) {
    return request.clone(
      {
        headers: new HttpHeaders({
          'Authorization': 'Bearer ' + loggedUser.token,
          'x-api-key': 'f6534839-ec89-4176-bea5-c6af12ac0704'
        }),
        responseType: 'json'
      });
  }
}
