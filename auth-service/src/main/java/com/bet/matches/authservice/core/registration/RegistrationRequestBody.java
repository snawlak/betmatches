package com.bet.matches.authservice.core.registration;

import lombok.Data;

import javax.validation.constraints.Email;

@Data
public class RegistrationRequestBody {
    @Email
    private String email;
    private String username;
    private String password;
}
