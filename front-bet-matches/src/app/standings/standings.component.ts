import {Component, OnInit} from '@angular/core';
import {TeamService} from "../teams/team.service";
import {PlayerService} from "../players/player.service";
import {Country} from "../common/country.service";
import {AuthService} from "../auth/auth.service";
import {Subscription, throwError} from "rxjs";
import {Team} from "../teams/team.model";

@Component({
  selector: 'app-standings',
  templateUrl: './standings.component.html',
  styleUrls: ['./standings.component.css']
})
export class StandingsComponent implements OnInit {
  private loggedUserSub: Subscription;
  userId: string = null;
  isFetching: boolean;
  teams: Team [] = [];


  constructor(private teamService: TeamService,
              private playerService: PlayerService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.isFetching = true;
    this.loggedUserSub = this.loggedUserSub = this.authService.loggedUser.subscribe(user => {
        const isAuthenticated = !!user;
        if (isAuthenticated) {
          this.userId = user.id
        }
      }
    );

    this.fetchTeams();

  }

  private fetchTeams() {
    this.teamService.fetchTeams().subscribe(teams => {
      this.teams = teams.sort((team1, team2) => {
        if (team1.place > team2.place) {
          return 1;
        }
        if (team1.place < team2.place) {
          return -1;
        }
        return 0;
      });
      this.isFetching = false;
    }, error => {
      this.isFetching = false;
      throwError(error);
    });
  }

}
