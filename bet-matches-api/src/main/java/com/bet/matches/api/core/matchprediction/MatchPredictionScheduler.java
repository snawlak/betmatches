package com.bet.matches.api.core.matchprediction;

import com.bet.matches.api.core.footballleague.FootballLeagueService;
import com.bet.matches.api.core.match.Match;
import com.bet.matches.api.core.match.MatchService;
import com.bet.matches.api.core.match.MatchStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class MatchPredictionScheduler {

    private final MatchService matchService;
    private final MatchPredictionService matchPredictionService;
    private final FootballLeagueService footballLeagueService;

    @Value("${bet.matches.football-league.spain.id}")
    private Integer footballLeagueId;

    @Scheduled(cron = "0 1 * * *")
    public void getNewMatchPredictions() {
        final int currentMatchDay = footballLeagueService.getCurrentMatchDay(footballLeagueId);
        final int nextMatchDay = currentMatchDay + 1;

        final List<Match> allMatchesFromCurrentMatchDay = matchService.getAllMatchesFromMatchDay(currentMatchDay)
                .stream()
                .filter(match -> MatchStatus.NOT_STARTED == match.getStatus())
                .collect(Collectors.toList());
        final List<Match> allMatchesFromNextMatchDay = matchService.getAllMatchesFromMatchDay(nextMatchDay);

        if (!allMatchesFromCurrentMatchDay.isEmpty()) {
            allMatchesFromCurrentMatchDay.forEach(this::updateMatchPrediction);
        }
        allMatchesFromNextMatchDay.forEach(this::updateMatchPrediction);
    }

    private void updateMatchPrediction(final Match match) {
        final int id = match.getId();
        matchPredictionService.updateMatchPrediction(id, match);
    }
}
