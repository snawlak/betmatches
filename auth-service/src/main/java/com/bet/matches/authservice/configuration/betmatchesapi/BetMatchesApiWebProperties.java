package com.bet.matches.authservice.configuration.betmatchesapi;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "bet.matches.api")
public class BetMatchesApiWebProperties {
    private String addUserUrl;
    private final List<Header> headers;

    @Data
    public static class Header {
        private final String key;
        private final String value;
    }
}
