package com.bet.matches.authservice.core.common.keycloak;

import com.bet.matches.authservice.configuration.keycloak.KeycloakWebProperties;
import com.bet.matches.authservice.core.login.KeycloakLoginResponseBody;
import com.bet.matches.authservice.core.login.LoginRequestBody;
import com.bet.matches.authservice.core.registration.RegistrationRequestBody;
import lombok.RequiredArgsConstructor;
import org.apache.http.HttpStatus;
import org.keycloak.TokenVerifier;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.BodyInserters.FormInserter;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.UUID;

import static com.bet.matches.authservice.core.common.keycloak.KeycloakUtils.getUserIdFromResponse;

@Service
@RequiredArgsConstructor
public class KeycloakService {

    private static final String ERROR_MSG = "User has not been registered!";

    private final KeycloakWebProperties keycloakWebProperties;
    private final WebClient authWebClient;
    private final Keycloak keycloak;

    private RealmResource realm;
    private String userRole;
    private String clientId;
    private String clientSecret;
    private String grandType;
    private String tokenUri;

    @PostConstruct
    private void init() {
        final String betMatchesApiRealm = keycloakWebProperties.getRealm().getBetMatchesApi();
        realm = keycloak.realm(betMatchesApiRealm);
        userRole = keycloakWebProperties.getRealm().getUserRole();
        clientId = keycloakWebProperties.getTokenClientId();
        clientSecret = keycloakWebProperties.getClientSecret();
        grandType = keycloakWebProperties.getGrandType();
        tokenUri = keycloakWebProperties.getTokenUri();
    }

    public UUID registerUser(final RegistrationRequestBody registrationRequestBody) {
        final Response response = register(registrationRequestBody);

        if (response.getStatus() == HttpStatus.SC_CREATED) {
            final UUID userId = getUserIdFromResponse(response);
            setRole(userId);
            return userId;
        }

        throw new IllegalStateException(ERROR_MSG);
    }

    public KeycloakLoginResponseBody loginUser(final LoginRequestBody loginRequestBody) {
        final FormInserter<String> requestBody = getBody(loginRequestBody);

        return login(requestBody);
    }

    public UUID getUserIdFromToken(final String token) throws VerificationException {
        final AccessToken accessToken = TokenVerifier
                .create(token, AccessToken.class)
                .getToken();

        final String subject = accessToken.getSubject();
        final UUID id = UUID.fromString(subject);
        return id;
    }

    private Response register(final RegistrationRequestBody registrationRequestBody) {
        final String username = registrationRequestBody.getUsername();
        final String password = registrationRequestBody.getPassword();
        final String email = registrationRequestBody.getEmail();

        final KeycloakUser keycloakUser = new KeycloakUser(username, password, email);

        return realm.users()
                .create(keycloakUser);
    }

    private KeycloakLoginResponseBody login(final FormInserter<String> requestBody) {
        return authWebClient
                .post()
                .uri(tokenUri)
                .body(requestBody)
                .retrieve()
                .bodyToMono(KeycloakLoginResponseBody.class)
                .block();
    }

    private void setRole(final UUID userId) {
        final String id = userId.toString();

        final RoleRepresentation roleRepresentation = realm.roles()
                .get(userRole)
                .toRepresentation();

        realm.users()
                .get(id)
                .roles()
                .realmLevel()
                .add(Collections.singletonList(roleRepresentation));
    }

    private FormInserter<String> getBody(final LoginRequestBody loginRequestBody) {
        final String username = loginRequestBody.getUsername();
        final String password = loginRequestBody.getPassword();

        return BodyInserters.fromFormData("client_id", clientId)
                .with("username", username)
                .with("password", password)
                .with("grant_type", grandType)
                .with("client_secret", clientSecret);
    }
}
