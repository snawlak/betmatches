package com.bet.matches.api.presentation;

import com.bet.matches.api.core.FootballLeagueDto;
import com.bet.matches.api.core.PlayerDto;
import com.bet.matches.api.core.footballleague.FootballLeagueService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("football-leagues")
public class FootballLeagueEndpoint {

    private final FootballLeagueService footballLeagueService;

    @GetMapping
    public List<FootballLeagueDto> getAllFootballLeagues() {
        return footballLeagueService.getAllFootballLeagues();
    }

    @GetMapping("/{id}")
    public FootballLeagueDto getFootballLeague(@PathVariable final int id) {
        return footballLeagueService.getFootballLeague(id);
    }

    @PostMapping
    public FootballLeagueDto addFootballLeague(@RequestBody final FootballLeagueDto footballLeagueDto) {
        return footballLeagueService.addFootballLeague(footballLeagueDto);
    }

    @PutMapping("/{id}")
    public FootballLeagueDto updateFootballLeague(@PathVariable final int id, @RequestBody final FootballLeagueDto footballLeagueDto) {
        return footballLeagueService.updateFootballLeague(id, footballLeagueDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Integer> deleteFootballLeague(@PathVariable final int id) {
        final boolean isDeleted = footballLeagueService.deleteFootballLeague(id);
        if (isDeleted) {
            return new ResponseEntity<>(id, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
