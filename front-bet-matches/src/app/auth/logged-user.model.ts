export class LoggedUser {
  constructor(
    public id: string,
    public username: string,
    public email: string,
    public points: number,
    private _token: string,
    private _tokenExpirationDate: Date) {
  }

  get token() {
    if (!this._tokenExpirationDate || new Date() > this._tokenExpirationDate) {
      return null;
    }
    return this._token;
  }

}
