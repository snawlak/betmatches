package com.bet.matches.api.core.stadium;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class StadiumRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private StadiumRepository stadiumRepository;

    @Before
    public void setup() {
        final Stadium stadium = new Stadium(1, "Stadium Poznan", "Poland", "Poznan", 42000);
        testEntityManager.persist(stadium);
        testEntityManager.flush();
    }

    @Test
    void getAllStadiums() {
        final Stadium stadium = new Stadium(1, "Stadium Poznan", "Poland", "Poznan", 42000);
        testEntityManager.persist(stadium);
        testEntityManager.flush();

        final List<Stadium> allStadiums = stadiumRepository.getAllStadiums();

        assertThat(allStadiums).isEqualTo(Collections.singletonList(stadium));
    }

    @Test
    void getStadium() {
    }

    @Test
    void addOrUpdateStadium() {
    }

    @Test
    void deleteStadium() {
    }
}
