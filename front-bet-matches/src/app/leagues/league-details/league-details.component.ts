import {Component, OnDestroy, OnInit} from '@angular/core';
import {League} from "../league.model";
import {ActivatedRoute, Params} from "@angular/router";
import {Subscription} from "rxjs";
import {LeagueService} from "../league.service";
import {User} from "../../users/user.model";
import {BetExtraService} from "../../core/bet-extra/bet-extra.service";
import {Player} from "../../players/player.model";
import {Team} from "../../teams/team.model";
import {BetExtra} from "../../core/bet-extra/bet-extra.model";
import {FootballLeagueService} from "../../core/football-league/football-league.service";
import {Match} from "../../matches/match.model";
import {MatchService} from "../../matches/match.service";
import {BetMatchResultsService} from "../../core/bet-match-results/bet-match-results.service";
import {BetMatchResult} from "../../core/bet-match-results/bet-match-results.model";

@Component({
  selector: 'app-league-details',
  templateUrl: './league-details.component.html',
  styleUrls: ['./league-details.component.css']
})
export class LeagueDetailsComponent implements OnInit, OnDestroy {
  paramsSubscription: Subscription;
  league: League;
  users: UserDetails [] = [];
  matches: Match [] = [];
  matchesFromMatchDay: Match [] = [];
  isFetching = false;
  userBetSelectedMatchDay;
  matchDays;

  constructor(private route: ActivatedRoute,
              private leagueService: LeagueService,
              private footballLeagueService: FootballLeagueService,
              private matchService: MatchService,
              private betMatchResultsService: BetMatchResultsService,
              private betExtraService: BetExtraService) {
  }

  ngOnInit(): void {
    this.isFetching = true;
    this.makeSubscription(this.route.params);

    this.footballLeagueService.getFootballLeague().subscribe(
      footballLeague => {
        this.userBetSelectedMatchDay = footballLeague.current_match_day.toString();
        this.matchDays = footballLeague.match_days
      }
    );

    this.matchService.fetchAllMatches().subscribe(
      matches => {
        this.matches = matches;
        this.isFetching = false;
      }
    );
  }

  ngOnDestroy(): void {
    this.paramsSubscription.unsubscribe();
  }

  private getUsers(league: League) {
    league.users.forEach(user => this.getUserDetails(user))
  }

  private makeSubscription(params) {
    this.paramsSubscription = params
      .subscribe(
        (params: Params) => {
          const leagueId = +params['id'];
          this.users = [];
          console.log(this.users);
          this.getLeague(leagueId);
        }
      );
  }

  getLeague(leagueId: number) {
    this.leagueService.getLeague(leagueId)
      .subscribe(
        league => {
          this.league = league;
          this.getUsers(this.league);
          // this.isFetching = false;
        }
      );
  }

  private getUserDetails(user: User) {
    this.betExtraService.getUsersBetExtra(user.id)
      .subscribe(betExtra => {
        this.betMatchResultsService.fetchBetMatchResults(user.id).subscribe(
          (betMatchResults) =>
            this.users.push(LeagueDetailsComponent.createUserDetails(betExtra, user, betMatchResults))
        )
      });
  }

  private static createUserDetails(betExtra: BetExtra, user: User, betMatchResults: BetMatchResult []): UserDetails {
    const betTeam = betExtra.team;
    const betPlayer = betExtra.player;

    return {
      id: user.id,
      username: user.username,
      betTeam: betTeam,
      betPlayer: betPlayer,
      points: user.points,
      userSelected: false,
      betMatchResults: betMatchResults
    };
  }

  getUsersSorted(users: UserDetails[]) {
    return users.sort((user1, user2) => {
      if (user1.points > user2.points) {
        return -1;
      }
      if (user1.points < user2.points) {
        return 1;
      }
      return 0;
    })
  }

  getUsersBetTeamName(betTeam: Team) {
    if (betTeam === null) {
      return 'None'
    }
    return betTeam.name;
  }

  getUsersBetTeamLogo(betTeam: Team) {
    if (betTeam === null) {
      return 'None'
    }
    return betTeam.logo;
  }

  getUsersBetPlayer(betPlayer: Player) {
    if (betPlayer === null) {
      return 'None'
    }
    return betPlayer.last_name;
  }

  getMatchDays() {
    return Array(this.matchDays)
      .fill(0)
      .map((x, i) => i + 1);
  }

  getMatchesFromMatchDay(matchDay) {
    this.matchesFromMatchDay = this.matches.filter(match => match.match_day === +matchDay);
    return this.matchesFromMatchDay;
  }

  getUserBetMatchTeamFirstScore(match: Match, userId: string) {
    const betMatchResult = this.users.find(user => user.id === userId)
      .betMatchResults.find(betMatchResult => betMatchResult.betMatchResultId.match === match.id);

    if (betMatchResult !== undefined) {
      const teamFirstScore = betMatchResult.team_first_score;
      return teamFirstScore !== null ? teamFirstScore : '?';

    }
  }

  getUserBetMatchTeamSecondScore(match: Match, userId: string) {
    const betMatchResult = this.users.find(user => user.id === userId)
      .betMatchResults.find(betMatchResult => betMatchResult.betMatchResultId.match === match.id);

    if (betMatchResult !== undefined) {
      const teamSecondScore = betMatchResult.team_second_score;
      return teamSecondScore !== null ? teamSecondScore : '?';

    }
  }

  showMatchResult(match: Match) {
    return new Date(match.match_date).getTime() <= new Date().getTime();
  }

  userSelected(id: string) {
    const user = this.users.find(user => user.id === id);
    user.userSelected = true;
  }

  getBetMatchResultIcon(userId: string, match: Match) {
    const betMatchResult = this.users.find(user => user.id === userId)
      .betMatchResults.find(betMatchResult => betMatchResult.betMatchResultId.match === match.id);

    if (betMatchResult !== undefined && betMatchResult.points_achieved != null && match.status === "FINISHED") {
      const pointsAchieved = betMatchResult.points_achieved;
      if (pointsAchieved === 3) {
        return "assets/images/win.svg";
      } else if (pointsAchieved === 1) {
        return "assets/images/draw.svg";
      } else {
        return "assets/images/defeat.svg";
      }
    } else {
      return "assets/images/draw.svg";
    }

  }
}

export class UserDetails {
  id: string;
  username: string;
  betPlayer: Player;
  betTeam: Team;
  points: number;
  userSelected: boolean;
  betMatchResults: BetMatchResult[];
}
