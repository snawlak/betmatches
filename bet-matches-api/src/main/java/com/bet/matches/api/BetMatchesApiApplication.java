package com.bet.matches.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BetMatchesApiApplication {

    public static void main(final String[] args) {
        SpringApplication.run(BetMatchesApiApplication.class, args);
    }

}
