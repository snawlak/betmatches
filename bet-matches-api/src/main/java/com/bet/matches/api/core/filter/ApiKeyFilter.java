package com.bet.matches.api.core.filter;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HttpMethod;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class ApiKeyFilter implements Filter {

    private static final String ERROR_MSG = "ApiKey is wrong or is missing";

    private final ApiKey apiKey;

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        final HttpServletResponse httpResponse = (HttpServletResponse) response;

        if (canRetrieveRequest(httpRequest)) {
            chain.doFilter(request, response);
        } else {
            httpResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
            httpResponse.getWriter().write(ERROR_MSG);
        }
    }

    private boolean canRetrieveRequest(final HttpServletRequest httpRequest) {
        final String requestMethod = httpRequest.getMethod();
        final String requestURI = httpRequest.getRequestURI();

        if (isDataMangerRequest(requestURI)) {
            return canRetrieveDataManagerRequest(httpRequest, requestMethod);
        } else if (isUserRequest(requestURI)) {
            return canRetrieveUserRequest(httpRequest, requestMethod);
        } else {
            return true;
        }
    }

    private boolean isDataMangerRequest(final String requestUri) {
        for (final String endpoint : apiKey.getDataManagerEndpoints()) {
            if (requestUri.equals(endpoint)) {
                return true;
            }
        }
        return false;
    }

    private boolean canRetrieveDataManagerRequest(final HttpServletRequest httpRequest, final String requestMethod) {
        if (isDataChangeRequest(requestMethod)) {
            final String apiKeyDataManager = apiKey.getDataManager();
            return isApiKeyValid(httpRequest, apiKeyDataManager);
        }

        return true;
    }

    private boolean isDataChangeRequest(final String requestMethod) {
        return requestMethod.equals(HttpMethod.POST)
                || requestMethod.equals(HttpMethod.PUT)
                || requestMethod.equals(HttpMethod.DELETE);
    }

    private boolean isUserRequest(final String requestURI) {
        return requestURI.equals(apiKey.getUserEndpoint());
    }

    private boolean canRetrieveUserRequest(final HttpServletRequest httpRequest, final String requestMethod) {
        if (isCreateUserRequest(requestMethod)) {
            final String apiKeyDataManager = apiKey.getCreateUser();
            return isApiKeyValid(httpRequest, apiKeyDataManager);
        } else if (isGetAllUsersRequest(requestMethod)) {
            final String apiKeyDataManager = apiKey.getAllUsers();
            return isApiKeyValid(httpRequest, apiKeyDataManager);
        }

        return true;
    }

    private boolean isCreateUserRequest(final String requestMethod) {
        return requestMethod.equals(HttpMethod.POST);
    }

    private boolean isGetAllUsersRequest(final String requestMethod) {
        return requestMethod.equals(HttpMethod.GET);
    }

    private boolean isApiKeyValid(final HttpServletRequest httpRequest, final String validApiKey) {
        final String headerKey = apiKey.getHeaderKey();
        final String apiKeyToCheck = httpRequest.getHeader(headerKey);

        return apiKeyToCheck.equals(validApiKey);
    }

}
