import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from "../auth/auth.service";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";
import {LoggedUser} from "../auth/logged-user.model";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private loggedUserSub: Subscription;
  isAuthenticated = false;
  user: LoggedUser = null;

  constructor(private router: Router, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.loggedUserSub = this.authService.loggedUser.subscribe(user => {
        this.isAuthenticated = !!user;
        if (this.isAuthenticated) {
          this.user = user;
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.loggedUserSub.unsubscribe();
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['/auth']);
  }
}
