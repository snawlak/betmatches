import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {BehaviorSubject, Subject} from "rxjs";
import {LoggedUser} from "./logged-user.model";
import {tap} from "rxjs/operators";
import {log} from "util";

interface AuthResponse {
  id: string,
  username: string,
  email: string,
  points: number,
  access_token: string;
  expires_in: number;
}

@Injectable({providedIn: 'root'})
export class AuthService {
  loggedUser = new BehaviorSubject<LoggedUser>(null);
  private signUpUrl = environment.signUpUrl;
  private loginUrl = environment.loginUrl;
  private localStorageUserDataKey: string = environment.signUpUrl;
  private tokenExpirationTimer: any;

  constructor(private http: HttpClient) {
  }

  signUp(username: string, password: string, email: string) {
    return this.http
      .post(
        this.signUpUrl,
        this.getSignUpBody(username, password, email)
      )
  }

  login(username: string, password: string) {
    return this.http
      .post<AuthResponse>(
        this.loginUrl,
        this.getLoginBody(username, password)
      ).pipe(
        tap(response => {
          this.handleAuthentication(
            response.id,
            response.username,
            response.email,
            +response.points,
            response.access_token,
            +response.expires_in);
        }))
  }

  autoLogin() {
    let loggedUserData: {
      id: string,
      username: string,
      email: string,
      points: number,
      _token: string;
      _tokenExpirationDate: string;
    } = JSON.parse(localStorage.getItem(this.localStorageUserDataKey));
    if (!loggedUserData) {
      return;
    }

    const loggedUser = new LoggedUser(
      loggedUserData.id,
      loggedUserData.username,
      loggedUserData.email,
      loggedUserData.points,
      loggedUserData._token,
      new Date(loggedUserData._tokenExpirationDate)
    );

    if (loggedUser.token) {
      this.loggedUser.next(loggedUser);
      const expirationDuration = new Date(loggedUserData._tokenExpirationDate).getTime() - new Date().getTime();
      this.autoLogout(expirationDuration);
    }
  }

  logout() {
    this.loggedUser.next(null);
    localStorage.removeItem(this.localStorageUserDataKey);

    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
    }

    this.tokenExpirationTimer = null;
  }

  autoLogout(expirationDuration: number) {
    this.tokenExpirationTimer = setTimeout(() => {
      this.logout();
    }, expirationDuration)
  }

  getSignUpBody(username: string, password: string, email: string) {
    return new HttpParams()
      .set('username', username)
      .set('password', password)
      .set('email', email)
      .toString()
  }

  getLoginBody(username: string, password: string) {
    return new HttpParams()
      .set('username', username)
      .set('password', password)
      .toString();
  }

  private handleAuthentication(id: string,
                               username: string,
                               email: string,
                               points: number,
                               accessToken: string,
                               expiresIn: number) {
    const tokenExpirationDate = new Date(new Date().getTime() + expiresIn * 1000);
    const loggedUser = new LoggedUser(id, username, email, points, accessToken, tokenExpirationDate);
    this.loggedUser.next(loggedUser);
    this.autoLogout(expiresIn * 1000);
    localStorage.setItem(this.localStorageUserDataKey, JSON.stringify(loggedUser));
  }
}
