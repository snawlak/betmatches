package com.bet.matches.authservice.core.login;

import com.bet.matches.authservice.core.common.betmatchesapi.BetMatchesApiService;
import com.bet.matches.authservice.core.common.betmatchesapi.User;
import com.bet.matches.authservice.core.common.keycloak.KeycloakService;
import lombok.RequiredArgsConstructor;
import org.keycloak.common.VerificationException;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static com.bet.matches.authservice.core.login.LoginResponseBodyMapper.toLoginResponseBody;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final KeycloakService keycloakService;
    private final BetMatchesApiService betMatchesApiService;

    public LoginResponseBody loginUser(final LoginRequestBody loginRequestBody) throws VerificationException {
        final KeycloakLoginResponseBody keycloakLoginResponseBody = keycloakService.loginUser(loginRequestBody);

        final String accessToken = keycloakLoginResponseBody.getAccessToken();
        final UUID userId = keycloakService.getUserIdFromToken(accessToken);

        final User user = betMatchesApiService.getUser(userId, accessToken);
        final LoginResponseBody loginResponseBody = toLoginResponseBody(user, keycloakLoginResponseBody);

        return loginResponseBody;
    }
}
