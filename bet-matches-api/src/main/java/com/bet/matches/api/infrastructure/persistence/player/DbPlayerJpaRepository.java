package com.bet.matches.api.infrastructure.persistence.player;

import com.bet.matches.api.core.player.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

interface DbPlayerJpaRepository extends JpaRepository<Player, Integer> {
    List<Player> findAllByIdRapidApi(final int rapidApiId);

    @Query("select p from Player p where p.goals = (select max(goals) FROM Player )")
    List<Player> findAllWithMostGoals();
}
