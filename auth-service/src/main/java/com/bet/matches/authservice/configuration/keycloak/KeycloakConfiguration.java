package com.bet.matches.authservice.configuration.keycloak;

import lombok.RequiredArgsConstructor;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class KeycloakConfiguration {

    private final KeycloakWebProperties keycloakWebProperties;

    @Bean
    public Keycloak keycloak() {
        final String baseUrl = keycloakWebProperties.getBaseUrl();
        final String realmMaster = keycloakWebProperties.getRealm().getMaster();
        final String username = keycloakWebProperties.getUsername();
        final String password = keycloakWebProperties.getPassword();
        final String clientId = keycloakWebProperties.getClientId();
        final Integer connectionPoolSize = keycloakWebProperties.getConnectionPoolSize();

        return KeycloakBuilder.builder()
                .serverUrl(baseUrl)
                .realm(realmMaster)
                .username(username)
                .password(password)
                .clientId(clientId)
                .resteasyClient(new ResteasyClientBuilder()
                                        .connectionPoolSize(connectionPoolSize)
                                        .build())
                .build();
    }
}
