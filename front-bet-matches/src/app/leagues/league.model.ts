import {User} from "../users/user.model";

export interface League {
  id?: number;
  name: string;
  description: string;
  users?: User[];
}
