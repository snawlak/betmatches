package com.bet.matches.authservice.core.registration;

import com.bet.matches.authservice.core.common.betmatchesapi.BetMatchesApiService;
import com.bet.matches.authservice.core.common.betmatchesapi.User;
import com.bet.matches.authservice.core.common.keycloak.KeycloakService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class RegistrationService {

    private final KeycloakService keycloakService;
    private final BetMatchesApiService betMatchesApiService;

    public User registerUser(final RegistrationRequestBody registrationRequestBody) {
        final UUID userId = keycloakService.registerUser(registrationRequestBody);
        return betMatchesApiService.registerUser(registrationRequestBody, userId);
    }
}
