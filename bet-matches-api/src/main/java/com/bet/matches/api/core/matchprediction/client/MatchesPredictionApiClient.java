package com.bet.matches.api.core.matchprediction.client;

import com.bet.matches.api.core.matchprediction.MatchPrediction;
import com.bet.matches.api.core.team.Team;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;

import java.net.URI;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class MatchesPredictionApiClient {

    private final WebClient matchesPredictionApiWebClient;

    public MatchPrediction getMatchPrediction(final Team teamFirst, final Team teamSecond) {
        return matchesPredictionApiWebClient
                .get()
                .uri(toGetPredictionUri(teamFirst, teamSecond))
                .retrieve()
                .bodyToMono(MatchPrediction.class)
                .block();
    }

    private static Function<UriBuilder, URI> toGetPredictionUri(final Team teamFirst, final Team teamSecond) {
        return uriBuilder -> uriBuilder
                .queryParam("team_first", teamFirst.getIdRapidApi())
                .queryParam("team_second", teamSecond.getIdRapidApi())
                .build();
    }
}
