package com.bet.matches.api.core.footballleague;

import com.bet.matches.api.core.FootballLeagueDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FootballLeagueService {

    private final FootballLeagueRepository repository;
    private final ModelMapper modelMapper;

    public List<FootballLeagueDto> getAllFootballLeagues() {
        return repository.getAllFootballLeagues().stream()
                .map(footballLeague -> modelMapper.map(footballLeague, FootballLeagueDto.class))
                .collect(Collectors.toList());
    }

    public FootballLeagueDto getFootballLeague(final int id) {
        return repository.getFootballLeague(id)
                .map(footballLeague -> modelMapper.map(footballLeague, FootballLeagueDto.class))
                .orElseThrow(IllegalStateException::new);
    }

    public FootballLeagueDto addFootballLeague(final FootballLeagueDto footballLeagueDto) {
        FootballLeague footballLeague = modelMapper.map(footballLeagueDto, FootballLeague.class);
        footballLeague = repository.addOrUpdateFootballLeague(footballLeague);
        return modelMapper.map(footballLeague, FootballLeagueDto.class);
    }

    @Transactional
    public FootballLeagueDto updateFootballLeague(final int id, final FootballLeagueDto newFootballLeague) {
        final FootballLeague footballLeague = repository.getFootballLeague(id)
                .orElseThrow(IllegalStateException::new);

        footballLeague.setIdRapidApi(newFootballLeague.getIdRapidApi());
        footballLeague.setCountry(newFootballLeague.getCountry());
        footballLeague.setFlag(newFootballLeague.getFlag());
        footballLeague.setLeagueName(newFootballLeague.getLeagueName());
        footballLeague.setLogo(newFootballLeague.getLogo());
        footballLeague.setTeamsNumber(newFootballLeague.getTeamsNumber());
        footballLeague.setMatchDays(newFootballLeague.getMatchDays());
        footballLeague.setCurrentMatchDay(newFootballLeague.getCurrentMatchDay());
        footballLeague.setStartDate(newFootballLeague.getStartDate());
        footballLeague.setEndDate(newFootballLeague.getEndDate());

        return modelMapper.map(footballLeague, FootballLeagueDto.class);
    }

    @Transactional
    public void setNextMatchDay(final int id) {
        final FootballLeague footballLeague = repository.getFootballLeague(id)
                .orElseThrow(IllegalStateException::new);

        final int nextMatchDay = footballLeague.getCurrentMatchDay() + 1;

        footballLeague.setCurrentMatchDay(nextMatchDay);
    }

    public boolean deleteFootballLeague(final int id) {
        if (repository.getFootballLeague(id).isPresent()) {
            repository.deleteFootballLeague(id);
            return true;
        }
        return false;
    }

    public int getMatchDays(final int id) {
        return repository.getFootballLeague(id)
                .orElseThrow(IllegalStateException::new)
                .getMatchDays();
    }

    public int getCurrentMatchDay(final int id) {
        return repository.getFootballLeague(id)
                .orElseThrow(IllegalStateException::new)
                .getCurrentMatchDay();
    }
}
