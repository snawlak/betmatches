export interface BetMatchResult {
  betMatchResultId: BetMatchResultId;
  team_first_score: number;
  team_second_score: number;
  points_achieved: number;
}

export interface BetMatchResultId {
  user: string;
  match: number;
}
