import {Component, OnDestroy, OnInit} from '@angular/core';
import {TeamService} from '../../teams/team.service';
import {Team} from '../../teams/team.model';
import {BetExtraService} from "../../core/bet-extra/bet-extra.service";
import {BetExtra} from "../../core/bet-extra/bet-extra.model";
import {Subscription, throwError} from "rxjs";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-bet-best-team',
  templateUrl: './bet-best-team.component.html',
  styleUrls: ['./bet-best-team.component.css']
})
export class BetBestTeamComponent implements OnInit, OnDestroy {
  private loggedUserSub: Subscription;
  teams: Team[] = [];
  selectedTeam: Team;
  betTeam: Team;
  isFetching = false;
  editMode = false;
  isBestTeamBet = false;
  userId: string = null;

  constructor(private teamService: TeamService,
              private betExtraService: BetExtraService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.isFetching = true;
    this.loggedUserSub = this.loggedUserSub = this.authService.loggedUser.subscribe(user => {
        const isAuthenticated = !!user;
        if (isAuthenticated) {
          this.userId = user.id;
          if (this.betTeam !== null) {
            this.getBetBestTeam();
          }
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.loggedUserSub.unsubscribe();
  }

  private getBetBestTeam() {
    this.betExtraService.getBetTeamOfUser(this.userId).subscribe(
      team => {
        this.betTeam = team;
        this.isBestTeamBet = this.betTeam !== null;
        this.editMode = !this.isBestTeamBet;
        if (this.editMode) {
          this.fetchTeams();
        }
      }
    );
  }

  private fetchTeams() {
    this.isFetching = true;
    this.teamService.fetchTeams().subscribe(teams => {
      this.teams = teams.sort((team1, team2) => team1.name.localeCompare(team2.name));
      this.isFetching = false;
    }, error => {
      this.isFetching = false;
      throwError(error);
    });
  }

  onSaveBetTeam(team: Team) {
    const betExtra: BetExtra = {team: team};
    this.betExtraService.updateBetExtra(this.userId, betExtra);
    this.editMode = false;
    this.isBestTeamBet = true;
    this.betTeam = team;
  }

  onChangeBetBestTeam() {
    this.editMode = true;
    if (this.teams.length === 0) {
      this.fetchTeams();
    }
  }

  // TODO: CHECK DATE - IF FOOTBALL LEAGUE STARTED THEN...
  isAbleToChange() {
    return true;
  }
}
