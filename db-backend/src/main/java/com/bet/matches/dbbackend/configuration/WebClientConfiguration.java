package com.bet.matches.dbbackend.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.function.Consumer;

@Configuration
@RequiredArgsConstructor
public class WebClientConfiguration {

    private final RapidApiWeb rapidApiWeb;
    private final BetMatchesApiWeb betMatchesApiWeb;

    @Bean
    public WebClient rapidApiWebClient() {
        return WebClient
                .builder()
                .baseUrl(rapidApiWeb.getBaseUrl())
                .defaultHeaders(toRapidApiHeaders(rapidApiWeb.getHeaders()))
                .build();
    }

    @Bean
    public WebClient betMatchesWebClient() {
        return WebClient
                .builder()
                .baseUrl(betMatchesApiWeb.getBaseUrl())
                .defaultHeaders(toBetMatchesHeaders(betMatchesApiWeb.getHeaders()))
                .build();
    }

    private static Consumer<HttpHeaders> toRapidApiHeaders(final List<RapidApiWeb.Header> headers) {
        return httpHeaders -> headers
                .forEach(header -> httpHeaders
                        .add(header.getKey(), header.getValue()));
    }

    private static Consumer<HttpHeaders> toBetMatchesHeaders(final List<BetMatchesApiWeb.Header> headers) {
        return httpHeaders -> headers
                .forEach(header -> httpHeaders
                        .add(header.getKey(), header.getValue()));
    }
}
