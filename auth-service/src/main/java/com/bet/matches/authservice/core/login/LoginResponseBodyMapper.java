package com.bet.matches.authservice.core.login;

import com.bet.matches.authservice.core.common.betmatchesapi.User;
import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class LoginResponseBodyMapper {

    public static LoginResponseBody toLoginResponseBody(final User user,
                                                        final KeycloakLoginResponseBody keycloakLoginResponseBody) {

        final UUID id = user.getId();
        final String username = user.getUsername();
        final String email = user.getEmail();
        final int points = user.getPoints();

        final String accessToken = keycloakLoginResponseBody.getAccessToken();
        final String expiresIn = keycloakLoginResponseBody.getExpiresIn();

        return LoginResponseBody.builder()
                .id(id)
                .username(username)
                .email(email)
                .points(points)
                .accessToken(accessToken)
                .expiresIn(expiresIn)
                .build();
    }
}
