export interface User {
  id: string; // TODO: uuid
  username: string;
  email: string;
  points: number;
}
