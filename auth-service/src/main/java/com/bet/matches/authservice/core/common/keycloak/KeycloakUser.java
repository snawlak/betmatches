package com.bet.matches.authservice.core.common.keycloak;

import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import javax.validation.constraints.Email;
import java.util.Collections;

public class KeycloakUser extends UserRepresentation {

    private final CredentialRepresentation credential = new CredentialRepresentation();

    public KeycloakUser(final String username, final String password, @Email final String email) {
        initCredentials(password);
        this.setUsername(username);
        this.setEmail(email);
        this.setCredentials(Collections.singletonList(credential));
        this.setEmailVerified(true);
        this.setEnabled(true);
    }

    private void initCredentials(final String password) {
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(password);
    }
}
