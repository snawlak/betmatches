import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Match} from './match.model';

@Injectable({providedIn: 'root'})
export class MatchService {
  private url = environment.betMatchesUrl + '/matches';

  constructor(private http: HttpClient) {
  }

  fetchAllMatches() {
    return this.http
      .get<Match[]>(
        this.url
      ).pipe(
        catchError(error => {
          return throwError(error);
        })
      );
  }


  fetchMatchesFromDate(date: Date) {
    const formattedDate = this.toOffsetDateTime(date);
    const uri = this.url + '?matchDate=' + formattedDate;
    return this.http
      .get<Match[]>(
        uri
      ).pipe(
        catchError(error => {
          return throwError(error);
        })
      );
  }

  toOffsetDateTime(date: Date): string {
    let month: string;
    let day: string;
    let hours: string;
    let minutes: string;
    let seconds: string;
    date.getMonth() < 10 ? month = '0' + (date.getMonth() + 1) : month = (date.getMonth() + 1).toString();
    date.getDate() < 10 ? day = '0' + date.getDate() : day = date.getDate().toString();
    date.getHours() < 10 ? hours = '0' + date.getHours() : hours = date.getHours().toString();
    date.getMinutes() < 10 ? minutes = '0' + date.getMinutes() : minutes = date.getMinutes().toString();
    date.getSeconds() < 10 ? seconds = '0' + date.getSeconds() : seconds = date.getSeconds().toString();
    return date.getFullYear() + '-' + month + '-' + day
      + 'T' + hours + ':' + minutes + ':' + seconds + '\%2B01:00';
  }

}
