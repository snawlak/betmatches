package com.bet.matches.authservice.configuration;

import com.bet.matches.authservice.configuration.betmatchesapi.BetMatchesApiWebProperties;
import com.bet.matches.authservice.configuration.keycloak.KeycloakWebProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.function.Consumer;

@Configuration
@RequiredArgsConstructor
public class WebClientConfiguration {

    private final BetMatchesApiWebProperties betMatchesApiWebProperties;
    private final KeycloakWebProperties keycloakWebProperties;

    @Bean
    public WebClient betMatchesApiWebClient() {
        final String addUserUrl = betMatchesApiWebProperties.getAddUserUrl();
        final Consumer<HttpHeaders> betMatchesHeaders = toBetMatchesHeaders(betMatchesApiWebProperties.getHeaders());

        return WebClient
                .builder()
                .baseUrl(addUserUrl)
                .defaultHeaders(betMatchesHeaders)
                .build();
    }

    @Bean
    public WebClient authWebClient() {
        final String baseUrl = keycloakWebProperties.getBaseUrl();

        return WebClient
                .builder()
                .baseUrl(baseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .build();
    }

    private static Consumer<HttpHeaders> toBetMatchesHeaders(final List<BetMatchesApiWebProperties.Header> headers) {
        return httpHeaders -> headers
                .forEach(header -> httpHeaders
                        .add(header.getKey(), header.getValue()));
    }
}
