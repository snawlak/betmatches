package com.bet.matches.dbbackend.core.rapidapi.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SeasonStatistics {

    @JsonProperty("matchsPlayed")
    private int matchesPlayed;

    private int win;

    private int draw;

    private int lose;

    private int goalsFor;

    private int goalsAgainst;
}
