package com.bet.matches.api.core.common.calculation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static com.bet.matches.api.core.common.calculation.MatchResult.getMatchResult;

@Component
@RequiredArgsConstructor
public class EndOfMatchCalc {

    private final Points points;

    private MatchResult actualMatchResult;
    private int actualTeamFirstScore;
    private int actualTeamSecondScore;
    private int betCorrectlyPoints;
    private int betWinningTeamOrDrawPoints;
    private boolean isInitialized = false;

    @PostConstruct
    private void init() {
        betCorrectlyPoints = points.getBetCorrectly();
        betWinningTeamOrDrawPoints = points.getBetWinningTeamOrDraw();
    }

    public void initializeCalculation(final int actualTeamFirstScore, final int actualTeamSecondScore) {
        this.actualTeamFirstScore = actualTeamFirstScore;
        this.actualTeamSecondScore = actualTeamSecondScore;
        actualMatchResult = getMatchResult(this.actualTeamFirstScore, this.actualTeamSecondScore);

        isInitialized = true;
    }

    public int calculateBetPoints(final int betTeamFirstScore, final int betTeamSecondScore) {
        if (isInitialized) {
            return getPoints(betTeamFirstScore, betTeamSecondScore);
        }
        throw new RuntimeException("Calculation has not been initialized");
    }

    private int getPoints(final int betTeamFirstScore, final int betTeamSecondScore) {
        if (betGoalsCorrectly(betTeamFirstScore, betTeamSecondScore)) {
            return betCorrectlyPoints;
        } else if (betWinningTeamOrDrawCorrectly(betTeamFirstScore, betTeamSecondScore)) {
            return betWinningTeamOrDrawPoints;
        } else {
            return 0;
        }
    }

    private boolean betGoalsCorrectly(final int betTeamFirstScore, final int betTeamSecondScore) {
        return betTeamFirstScore == actualTeamFirstScore && betTeamSecondScore == actualTeamSecondScore;
    }

    private boolean betWinningTeamOrDrawCorrectly(final int betTeamFirstScore, final int betTeamSecondScore) {
        final MatchResult betMatchResult = getMatchResult(betTeamFirstScore, betTeamSecondScore);
        return betMatchResult == actualMatchResult;
    }
}
