package com.bet.matches.api.infrastructure.persistence.matchprediction;

import com.bet.matches.api.core.match.Match;
import com.bet.matches.api.core.matchprediction.MatchPrediction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.OffsetDateTime;
import java.util.List;

interface DbMatchPredictionJpaRepository extends JpaRepository<MatchPrediction, Integer> {

}
