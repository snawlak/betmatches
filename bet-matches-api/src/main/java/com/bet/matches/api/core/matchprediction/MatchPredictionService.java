package com.bet.matches.api.core.matchprediction;

import com.bet.matches.api.core.match.Match;
import com.bet.matches.api.core.matchprediction.client.MatchesPredictionApiClient;
import com.bet.matches.api.core.team.Team;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class MatchPredictionService {

    private final MatchesPredictionApiClient matchesPredictionApiClient;
    private final MatchPredictionRepository matchPredictionRepository;

    public MatchPrediction addMatchPrediction(final Match match) {
        final MatchPrediction matchPrediction = getMatchPrediction(match);
        matchPrediction.setMatch(match);
        return matchPredictionRepository.addOrUpdateMatch(matchPrediction);
    }

    @Transactional
    public MatchPrediction updateMatchPrediction(final int id, final Match match) {
        final MatchPrediction matchPrediction = matchPredictionRepository.getMatchPrediction(id)
                .orElseThrow(IllegalStateException::new);
        final MatchPrediction newMatchPrediction = getMatchPrediction(match);

        matchPrediction.setTeamFirstWin(newMatchPrediction.getTeamFirstWin());
        matchPrediction.setDraw(newMatchPrediction.getDraw());
        matchPrediction.setTeamSecondWin(newMatchPrediction.getTeamSecondWin());
        matchPrediction.setTeamFirstGoals(newMatchPrediction.getTeamFirstGoals());
        matchPrediction.setTeamSecondGoals(newMatchPrediction.getTeamSecondGoals());

        return matchPrediction;
    }

    private MatchPrediction getMatchPrediction(final Match match) {
        final Team teamFirst = match.getTeamFirst();
        final Team teamSecond = match.getTeamSecond();
        return matchesPredictionApiClient.getMatchPrediction(teamFirst, teamSecond);
    }
}
