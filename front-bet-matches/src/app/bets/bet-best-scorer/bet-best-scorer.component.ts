import {Component, OnDestroy, OnInit} from '@angular/core';
import {Team} from '../../teams/team.model';
import {TeamService} from '../../teams/team.service';
import {PlayerService} from '../../players/player.service';
import {Player} from '../../players/player.model';
import {Country} from '../../common/country.service';
import {BetExtra} from "../../core/bet-extra/bet-extra.model";
import {BetExtraService} from "../../core/bet-extra/bet-extra.service";
import {Subscription, throwError} from "rxjs";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-bet-best-scorer',
  templateUrl: './bet-best-scorer.component.html',
  styleUrls: ['./bet-best-scorer.component.css']
})
export class BetBestScorerComponent implements OnInit, OnDestroy {
  private loggedUserSub: Subscription;
  teams: Team[] = [];
  players: Player[] = [];
  selectedTeamPlayers: Player[] = [];
  teamSelected: Team;
  betPlayer: Player;
  isTeamSelected = false;
  isFetching = false;
  isBestScorerBet = false;
  editMode = false;
  userId: string = null;
  playersPosition = ['ATTACKER', 'MIDFIELDER', 'DEFENDER', 'GOALKEEPER'];


  constructor(private teamService: TeamService,
              private playerService: PlayerService,
              private country: Country,
              private betExtraService: BetExtraService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.loggedUserSub = this.loggedUserSub = this.authService.loggedUser.subscribe(user => {
        const isAuthenticated = !!user;
        if (isAuthenticated) {
          this.userId = user.id
        }
      }
    );

    if (this.betPlayer !== null) {
      this.getBetBestScorer();
    }
  }

  ngOnDestroy(): void {
    this.loggedUserSub.unsubscribe();
  }

  private fetchTeams() {
    this.isFetching = true;
    this.teamService.fetchTeams().subscribe(teams => {
      this.teams = teams.sort((team1, team2) => team1.name.localeCompare(team2.name));
      this.isFetching = false;
    }, error => {
      this.isFetching = false;
      throwError(error);
    });
  }

  private fetchPlayers() {
    this.isFetching = true;
    this.playerService.fetchPlayers().subscribe(players => {
      this.players = players;
      this.isFetching = false;
      this.onTeamSelected(0);
    }, error => {
      this.isFetching = false;
      throwError(error);
    });
  }

  private getBetBestScorer() {
    this.betExtraService.getBetPlayerOfUser(this.userId).subscribe(
      player => {
        this.betPlayer = player;
        this.isBestScorerBet = this.betPlayer !== null;
        this.editMode = !this.isBestScorerBet;
        if (this.editMode) {
          this.fetchTeams();
          this.fetchPlayers();
        }
      }
    );
  }

  onTeamSelected(i: number) {
    this.selectedTeamPlayers = [];
    this.teamSelected = this.teams[i];
    this.selectedTeamPlayers = this.players.filter(
      player => player.team.id === this.teamSelected.id
    );

    this.isTeamSelected = true;
  }

  getFlag(nationality: string) {
    const code = this.country.getCountryCode(nationality);
    return 'https://media.api-football.com/flags/' + code.toLowerCase() + '.svg';
  }

  onSaveBetPlayer(player: Player) {
    const betExtra: BetExtra = {player: player};
    this.betExtraService.updateBetExtra(this.userId, betExtra);
    this.editMode = false;
    this.isBestScorerBet = true;
    this.betPlayer = player;
  }

  getPlayers(position: string) {
    return this.selectedTeamPlayers.filter(player => player.position === position);
  }

  isAbleToChange() {
    return true;
  }

  onChangeBetBestScorer() {
    this.editMode = true;
    if (this.teams.length === 0) {
      this.fetchTeams();
    }
    if (this.players.length === 0) {
      this.fetchPlayers();
    }
  }
}
