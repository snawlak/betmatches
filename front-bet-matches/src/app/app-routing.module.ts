import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TeamsComponent} from './teams/teams.component';
import {BetsComponent} from './bets/bets.component';
import {BetBestTeamComponent} from './bets/bet-best-team/bet-best-team.component';
import {BetBestScorerComponent} from './bets/bet-best-scorer/bet-best-scorer.component';
import {BetMatchesComponent} from './bets/bet-matches/bet-matches.component';
import {LeaguesComponent} from "./leagues/leagues.component";
import {LeagueDetailsComponent} from "./leagues/league-details/league-details.component";
import {JoinLeagueComponent} from "./leagues/join-league/join-league.component";
import {CreateLeagueComponent} from "./leagues/create-league/create-league.component";
import {AuthComponent} from "./auth/auth.component";
import {AuthGuard} from "./auth/auth.guard";
import {HomeComponent} from "./home/home.component";
import {StandingsComponent} from "./standings/standings.component";
import {RulesComponent} from "./rules/rules.component";


const appRoutes = [
  {
    path: 'bets', component: BetsComponent, canActivate: [AuthGuard], children: [
      {path: 'matches', component: BetMatchesComponent},
      {path: 'best-scorer', component: BetBestScorerComponent},
      {path: 'best-team', component: BetBestTeamComponent},
    ]
  },
  {
    path: 'leagues', component: LeaguesComponent, canActivate: [AuthGuard], children: [
      {path: ':id', component: LeagueDetailsComponent}]
  },
  {path: 'league/join', component: JoinLeagueComponent, canActivate: [AuthGuard]},
  {path: 'league/create', component: CreateLeagueComponent, canActivate: [AuthGuard]},
  {path: 'teams', component: TeamsComponent, canActivate: [AuthGuard]},
  {path: 'standings', component: StandingsComponent, canActivate: [AuthGuard]},
  {path: 'rules', component: RulesComponent},
  {path: 'auth', component: AuthComponent},
  {path: '', component: HomeComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
