package com.bet.matches.api.core;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Id;
import javax.validation.constraints.Email;
import java.util.UUID;

@Data
public class UserRequestBodyDto {

    @ApiModelProperty(example = "240f0f06-a8c8-11ea-bb37-0242ac130002")
    private UUID id;

    @ApiModelProperty(example = "superBob")
    private String username;

    @Email
    @ApiModelProperty(example = "superBob@gmail.com")
    private String email;
}
