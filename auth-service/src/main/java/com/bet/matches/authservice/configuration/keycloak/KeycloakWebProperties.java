package com.bet.matches.authservice.configuration.keycloak;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "auth.keycloak")
public class KeycloakWebProperties {

    private final Realm realm = new Realm();

    private String baseUrl;
    private String tokenUri;
    private String username;
    private String password;
    private Integer connectionPoolSize;
    private String clientId;
    private String clientSecret;
    private String tokenClientId;
    private String grandType;

    @Data
    public static class Realm {
        private String master;
        private String betMatchesApi;
        private String userRole;
    }
}
