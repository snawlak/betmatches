import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  isLoginMode = true;
  isLoading = false;
  error: string = null;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
  }

  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  onSubmit(authForm: NgForm) {
    if (!authForm.valid) {
      return;
    }
    const username = authForm.value.username;
    const password = authForm.value.password;
    const email = authForm.value.email;

    this.isLoading = true;
    if (this.isLoginMode) {
      this.login(username, password);
    } else {
      this.signUp(username, password, email);
    }
    authForm.reset();
  }

  private signUp(username: string, password: string, email: string) {
    this.authService.signUp(username, password, email).subscribe(
      response => {
        this.isLoading = false;
        this.isLoginMode = true;
      },
      error => {
        // TODO: advance error
        this.error = 'An error occured!';
        this.isLoading = false;
      });
  }

  private login(username: string, password: string) {
    this.authService.login(username, password).subscribe(
      response => {
        this.isLoading = false;
        this.router.navigate(['/leagues'])
      },
      error => {
        // TODO: advance error
        this.error = 'An error occured!';
        this.isLoading = false;
      });
  }
}
