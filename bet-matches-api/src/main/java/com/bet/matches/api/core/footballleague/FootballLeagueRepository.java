package com.bet.matches.api.core.footballleague;

import java.util.List;
import java.util.Optional;

public interface FootballLeagueRepository {

    List<FootballLeague> getAllFootballLeagues();

    Optional<FootballLeague> getFootballLeague(final int id);

    FootballLeague addOrUpdateFootballLeague(final FootballLeague stadium);

    void deleteFootballLeague(final int id);
}
