package com.bet.matches.dbbackend.core.betmatchesapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Team {

    private int id;

    @JsonProperty("id_rapid_api")
    private int idRapidApi;

    private String name;

    private String logo;

    private int place;

    private int win;

    private int draw;

    private int lose;

    @JsonProperty("goals_for")
    private int goalsFor;

    @JsonProperty("goals_against")
    private int goalsAgainst;

    private int points;

}
