import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LeagueService} from "../league.service";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-join-league',
  templateUrl: './join-league.component.html',
  styleUrls: ['./join-league.component.css']
})
export class JoinLeagueComponent implements OnInit, OnDestroy {
  private loggedUserSub: Subscription;
  joinLeagueForm: FormGroup;
  userId: string = null;

  constructor(private leagueService: LeagueService,
              private router: Router,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.loggedUserSub = this.loggedUserSub = this.authService.loggedUser.subscribe(user => {
        const isAuthenticated = !!user;
        if (isAuthenticated) {
          this.userId = user.id
        }
      }
    );

    this.joinLeagueForm = new FormGroup({
      'name': new FormControl(null, [Validators.required]),
      'leagueId': new FormControl(null, [Validators.required])
    });
  }

  ngOnDestroy(): void {
    this.loggedUserSub.unsubscribe();
  }

  onSubmit() {
    const leagueId = this.joinLeagueForm.get('leagueId').value;
    const nextRoot = '/leagues/' + leagueId;

    this.leagueService.addUserToLeague(leagueId, this.userId)
      .subscribe(() => this.router.navigate([nextRoot]));
  }
}
