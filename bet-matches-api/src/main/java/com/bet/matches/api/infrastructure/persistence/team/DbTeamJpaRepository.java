package com.bet.matches.api.infrastructure.persistence.team;

import com.bet.matches.api.core.team.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

interface DbTeamJpaRepository extends JpaRepository<Team, Integer> {
    @Query("select t from Team t where t.place = 1")
    Team findBestTeam();
}
