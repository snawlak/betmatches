import {Team} from '../teams/team.model';
import {Stadium} from '../common/stadium.model';
import {MatchPrediction} from "./match-prediction.model";

export interface Match {
  id: number;
  id_rapid_api: number;
  team_first: Team;
  team_second: Team;
  match_date: Date;
  match_day: number;
  status: string;
  team_first_score: number;
  team_second_score: number;
  stadium: Stadium;
  match_prediction: MatchPrediction;
}
