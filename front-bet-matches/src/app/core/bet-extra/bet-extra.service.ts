import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {throwError} from "rxjs";
import {BetExtra} from "./bet-extra.model";
import {Team} from "../../teams/team.model";
import {Player} from "../../players/player.model";
import {catchError, map} from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class BetExtraService {
  private url = environment.betMatchesUrl + '/bet-extra/';

  constructor(private http: HttpClient) {

  }

  updateBetExtra(userId: string, betExtra) {
    const fullUrl = this.url + userId;
    this.http
      .put<BetExtra>(
        fullUrl,
        betExtra
      ).subscribe(responseData => {
      console.log(responseData);
    }, error => {
      throwError(error)
    });
  }

  private getBetExtra(userId: string) {
    const fullUrl = this.url + userId;
    return this.http
      .get<BetExtra>(
        fullUrl
      );
  }

  getBetPlayerOfUser(userId: string) {
    return this.getBetExtra(userId)
      .pipe(
        map((betExtra) => {
          return betExtra.player
        }),
        catchError(err => {
          return throwError(err)
        })
      )
  }

  getBetTeamOfUser(userId: string) {
    return this.getBetExtra(userId)
      .pipe(
        map((betExtra) => {
          return betExtra.team
        }),
        catchError(err => {
          return throwError(err)
        })
      )
  }

  getUsersBetExtra(id: string) {
    const fullUrl = this.url + id;
    return this.http
      .get<BetExtra>(
        fullUrl
      ).pipe(
        map((betExtra) => {
          return betExtra
        }),
        catchError(err => {
          return throwError(err)
        })
      );
  }

  getUsersBetPlayer(id: string) {
    const fullUrl = this.url + id;
    return this.http
      .get<BetExtra>(
        fullUrl
      ).pipe(
        map((betExtra) => {
          return betExtra.team
        }),
        catchError(err => {
          return throwError(err)
        })
      );
  }


}
