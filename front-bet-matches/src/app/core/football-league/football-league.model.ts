export interface FootballLeague {
  id: number;
  id_rapid_api: number;
  country: string;
  flag: string;
  league_name: string;
  logo: string;
  teams_number: number;
  match_days: number;
  current_match_day: number;
  start_date: Date;
  end_date: Date;
}
