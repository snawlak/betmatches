import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {TeamsComponent} from './teams/teams.component';
import {AppRoutingModule} from './app-routing.module';
import {PlayersComponent} from './players/players.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MatchesComponent} from './matches/matches.component';
import {BetsComponent} from './bets/bets.component';
import {BetMatchesComponent} from './bets/bet-matches/bet-matches.component';
import {BetBestTeamComponent} from './bets/bet-best-team/bet-best-team.component';
import {BetBestScorerComponent} from './bets/bet-best-scorer/bet-best-scorer.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatRippleModule} from '@angular/material/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UsersComponent} from './users/users.component';
import {LeaguesComponent} from './leagues/leagues.component';
import {LeaguesListComponent} from './leagues/leagues-list/leagues-list.component';
import {LeagueDetailsComponent} from './leagues/league-details/league-details.component';
import {MatInputModule} from "@angular/material/input";
import {JoinLeagueComponent} from './leagues/join-league/join-league.component';
import {CreateLeagueComponent} from './leagues/create-league/create-league.component';
import {AuthComponent} from "./auth/auth.component";
import {LoadingSpinnerComponent} from "./common/loading-spinner/loading-spinner.component";
import {AuthInterceptorService} from "./auth/auth-interceptor.service";
import { HomeComponent } from './home/home.component';
import { StandingsComponent } from './standings/standings.component';
import { RulesComponent } from './rules/rules.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TeamsComponent,
    PlayersComponent,
    MatchesComponent,
    BetsComponent,
    BetMatchesComponent,
    BetBestTeamComponent,
    BetBestScorerComponent,
    UsersComponent,
    LeaguesComponent,
    LeaguesListComponent,
    LeagueDetailsComponent,
    JoinLeagueComponent,
    CreateLeagueComponent,
    AuthComponent,
    LoadingSpinnerComponent,
    HomeComponent,
    StandingsComponent,
    RulesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTableModule,
    MatRippleModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
