package com.bet.matches.dbbackend.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "bet.matches.api")
public class BetMatchesApiWeb {

    private final List<Header> headers;
    private final Endpoint endpoint = new Endpoint();
    private String baseUrl;

    @Data
    public static class Endpoint {
        private String teams;
        private String stadiums;
        private String players;
        private String matches;
    }

    @Data
    public static class Header {
        private final String key;
        private final String value;
    }

}

