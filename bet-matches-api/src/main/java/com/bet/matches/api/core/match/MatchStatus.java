package com.bet.matches.api.core.match;

public enum MatchStatus {
    NOT_STARTED,
    POSTPONED,
    LIVE,
    FINISHED;
}
