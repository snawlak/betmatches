package com.bet.matches.api.core.user;

import com.bet.matches.api.core.UserRequestBodyDto;
import com.bet.matches.api.core.league.League;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static lombok.AccessLevel.PROTECTED;

@Getter
@Setter
@Entity
@Table(name = "users")
@NoArgsConstructor(access = PROTECTED)
@Embeddable
public class User implements Serializable {

    @Id
    private UUID id;

    @Email
    @Column(nullable = false)
    private String email;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private int points;

    @Column(nullable = false)
    private boolean admin;

    @JsonIgnore
    @ManyToMany(mappedBy = "users")
    private Set<League> leagues;

    public User(final UserRequestBodyDto userRequestBodyDto) {
        this.id = userRequestBodyDto.getId();
        this.username = userRequestBodyDto.getUsername();
        this.email = userRequestBodyDto.getEmail();
        this.points = 0;
        this.admin = false;
    }

    public void addLeague(final League league) {
        if (leagues == null) {
            leagues = new HashSet<>();
        }
        leagues.add(league);
    }
}
