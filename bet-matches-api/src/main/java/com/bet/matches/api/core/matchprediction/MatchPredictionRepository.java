package com.bet.matches.api.core.matchprediction;

import java.util.Optional;

public interface MatchPredictionRepository {

    Optional<MatchPrediction> getMatchPrediction(final int id);

    MatchPrediction addOrUpdateMatch(final MatchPrediction matchPrediction);
}
