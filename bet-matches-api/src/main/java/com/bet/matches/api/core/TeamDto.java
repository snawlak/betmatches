package com.bet.matches.api.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TeamDto {

    @ApiModelProperty(example = "123")
    private int id;

    @JsonProperty("id_rapid_api")
    @ApiModelProperty(example = "123")
    private int idRapidApi;

    @ApiModelProperty(example = "Tottenham Hotspur")
    private String name;

    @ApiModelProperty(example = "https://logo.pl/123")
    private String logo;

    @ApiModelProperty(example = "1")
    private int place;

    @ApiModelProperty(example = "16")
    private int win;

    @ApiModelProperty(example = "2")
    private int draw;

    @ApiModelProperty(example = "10")
    private int lose;

    @JsonProperty("goals_for")
    @ApiModelProperty(example = "45")
    private int goalsFor;

    @JsonProperty("goals_against")
    @ApiModelProperty(example = "21")
    private int goalsAgainst;

    @ApiModelProperty(example = "54")
    private int points;
}
