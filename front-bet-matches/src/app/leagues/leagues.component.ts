import {Component, OnInit} from '@angular/core';
import {League} from "./league.model";
import {Subscription} from "rxjs";
import {LeagueService} from "./league.service";
import {AuthService} from "../auth/auth.service";

@Component({
  selector: 'app-leagues',
  templateUrl: './leagues.component.html',
  styleUrls: ['./leagues.component.css']
})
export class LeaguesComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
