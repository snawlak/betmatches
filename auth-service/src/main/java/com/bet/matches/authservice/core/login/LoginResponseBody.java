package com.bet.matches.authservice.core.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Builder
@Data
public class LoginResponseBody {
    private final UUID id;

    private final String username;

    private final String email;

    private final int points;

    @JsonProperty("access_token")
    private final String accessToken;

    @JsonProperty("expires_in")
    private final String expiresIn;
}
