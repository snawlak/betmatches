export interface Team {
  id: number;
  name: string;
  place: number;
  logo: string;
  win: number;
  draw: number;
  lose: number;
  goals_for: number;
  goals_against: number;
  points: number;
}
