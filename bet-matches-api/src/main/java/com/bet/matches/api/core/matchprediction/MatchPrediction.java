package com.bet.matches.api.core.matchprediction;

import com.bet.matches.api.core.match.Match;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "matches_prediction")
@NoArgsConstructor
@AllArgsConstructor
public class MatchPrediction {

    @Id
    @ApiModelProperty(example = "123")
    @Column(name = "id_match", updatable = false, nullable = false)
    private int id;

    @MapsId
    @OneToOne
    @JoinColumn(name = "id_match", referencedColumnName = "id_match")
    @ApiModelProperty(example = "123")
    private Match match;

    @Column(name = "prediction_draw", nullable = false)
    @JsonProperty("prediction_draw")
    private float draw;

    @Column(name = "prediction_team_first_win", nullable = false)
    @JsonProperty("prediction_team_first_win")
    private float teamFirstWin;

    @Column(name = "prediction_team_second_win", nullable = false)
    @JsonProperty("prediction_team_second_win")
    private float teamSecondWin;

    @Column(name = "prediction_team_first_goals", nullable = false)
    @JsonProperty("prediction_team_first_goals")
    private int teamFirstGoals;

    @Column(name = "prediction_team_second_goals", nullable = false)
    @JsonProperty("prediction_team_second_goals")
    private int teamSecondGoals;
}
