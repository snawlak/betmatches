package com.bet.matches.authservice.core.login;

import lombok.Data;

@Data
public class LoginRequestBody {
    private String username;
    private String password;
}
