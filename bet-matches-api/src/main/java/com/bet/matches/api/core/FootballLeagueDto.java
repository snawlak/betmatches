package com.bet.matches.api.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class FootballLeagueDto {

    @ApiModelProperty(example = "123")
    private int id;

    @ApiModelProperty(example = "123")
    @JsonProperty("id_rapid_api")
    private int idRapidApi;

    @ApiModelProperty(example = "England")
    private String country;

    @ApiModelProperty(example = "https://media.api-sports.io/flags/gb.svg")
    private String flag;

    @ApiModelProperty(example = "Premier League")
    @JsonProperty("league_name")
    private String leagueName;

    @ApiModelProperty(example = "https://media.api-sports.io/football/leagues/2.png")
    private String logo;

    @ApiModelProperty(example = "20")
    @JsonProperty("teams_number")
    private int teamsNumber;

    @ApiModelProperty(example = "38")
    @JsonProperty("match_days")
    private int matchDays;

    @ApiModelProperty(example = "20")
    @JsonProperty("current_match_day")
    private int currentMatchDay;

    @ApiModelProperty(example = "2019-08-01T10:07:06.407Z")
    @JsonProperty("start_date")
    private OffsetDateTime startDate;

    @ApiModelProperty(example = "2020-06-01T10:07:06.407Z")
    @JsonProperty("end_date")
    private OffsetDateTime endDate;
}
