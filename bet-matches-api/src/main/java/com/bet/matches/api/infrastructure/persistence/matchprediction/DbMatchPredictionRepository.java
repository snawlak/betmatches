package com.bet.matches.api.infrastructure.persistence.matchprediction;

import com.bet.matches.api.core.matchprediction.MatchPrediction;
import com.bet.matches.api.core.matchprediction.MatchPredictionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
class DbMatchPredictionRepository implements MatchPredictionRepository {

    private final DbMatchPredictionJpaRepository repository;

    @Override
    public Optional<MatchPrediction> getMatchPrediction(final int id) {
        return repository.findById(id);
    }

    @Override
    public MatchPrediction addOrUpdateMatch(final MatchPrediction matchPrediction) {
        return repository.save(matchPrediction);
    }
}
