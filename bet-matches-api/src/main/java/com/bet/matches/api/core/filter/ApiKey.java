package com.bet.matches.api.core.filter;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "api.key")
public class ApiKey {

    private String headerKey;
    private String createUser;
    private String allUsers;
    private String dataManager;

    private String userEndpoint;
    private List<String> dataManagerEndpoints;
}
