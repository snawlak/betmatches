package com.bet.matches.api.core.common.listener;

import com.bet.matches.api.core.betresults.BetMatchResult;
import com.bet.matches.api.core.betresults.BetMatchResultRepository;
import com.bet.matches.api.core.common.calculation.EndOfMatchCalc;
import com.bet.matches.api.core.common.event.OnEndOfMatchEvent;
import com.bet.matches.api.core.match.Match;
import com.bet.matches.api.core.user.User;
import com.bet.matches.api.core.user.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

@Slf4j
@Component
@RequiredArgsConstructor
public class OnEndOfMatchListener {

    private final EndOfMatchCalc endOfMatchCalc;
    private final BetMatchResultRepository betMatchResultRepository;
    private final UserRepository userRepository;

    @EventListener
    public void updateUserPoints(final OnEndOfMatchEvent event) {
        final Match match = event.getMatch();
        updateUsersPoints(match);
    }

    private void updateUsersPoints(final Match match) {
        final List<BetMatchResult> allBetResults =
                betMatchResultRepository.getAllBetMatchResultsOfSpecificMatch(match.getId());
        final int actualTeamFirstScore = match.getTeamFirstScore();
        final int actualTeamSecondScore = match.getTeamSecondScore();
        final Map<UUID, Integer> usersPoints = new HashMap<>();

        endOfMatchCalc.initializeCalculation(actualTeamFirstScore, actualTeamSecondScore);

        allBetResults.forEach(findUserWhichBetCorrectlyAndSetPoints(usersPoints));
        usersPoints.forEach(this::addPointsToUsers);
    }

    private Consumer<BetMatchResult> findUserWhichBetCorrectlyAndSetPoints(final Map<UUID, Integer> usersPoints) {
        return betMatchResult -> {
            final int betTeamFirstScore = betMatchResult.getTeamFirstScore();
            final int betTeamSecondScore = betMatchResult.getTeamSecondScore();
            final int pointsToAdd = endOfMatchCalc.calculateBetPoints(betTeamFirstScore, betTeamSecondScore);

            setPointsInBetMatchResult(betMatchResult, pointsToAdd);

            if (pointsToAdd != 0) {
                usersPoints.put(betMatchResult.getBetMatchResultId().getUser(), pointsToAdd);
            }
        };
    }

    private void setPointsInBetMatchResult(final BetMatchResult betMatchResult, final int pointsToAdd) {
        betMatchResult.setPointsAchieved(pointsToAdd);
        betMatchResultRepository.addOrUpdateBetMatchResult(betMatchResult);
    }


    private void addPointsToUsers(final UUID idUser, final Integer points) {
        final User user = this.userRepository.getUser(idUser)
                .orElseThrow(RuntimeException::new);
        final int currentPoints = user.getPoints();
        final int newPoints = currentPoints + points;

        user.setPoints(newPoints);

        this.userRepository.addOrUpdateUser(user);
    }
}
