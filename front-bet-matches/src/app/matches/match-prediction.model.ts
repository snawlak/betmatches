export interface MatchPrediction {

  prediction_draw: number;
  prediction_team_first_win: number;
  prediction_team_second_win: number;
  prediction_team_first_goals: number;
  prediction_team_second_goals: number;
}
