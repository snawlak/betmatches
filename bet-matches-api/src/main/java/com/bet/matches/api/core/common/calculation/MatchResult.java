package com.bet.matches.api.core.common.calculation;

enum MatchResult {
    FIRST_TEAM_WON,
    SECOND_TEAM_WON,
    DRAW;

    public static MatchResult getMatchResult(final int teamFirstScore, final int teamSecondScore) {
        if (teamFirstScore > teamSecondScore) {
            return MatchResult.FIRST_TEAM_WON;
        } else if (teamFirstScore < teamSecondScore) {
            return MatchResult.SECOND_TEAM_WON;
        } else {
            return MatchResult.DRAW;
        }
    }
}
