package com.bet.matches.api.core.matchprediction.client.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@RequiredArgsConstructor
public class MatchesPredictionWebClientConfiguration {

    private final MatchesPredictionApiWeb matchesPredictionApiWeb;

    @Bean
    public WebClient matchesPredictionApiWebClient() {
        return WebClient
                .builder()
                .defaultHeader("Content-Type", "application/json")
                .baseUrl(matchesPredictionApiWeb.getUrl())
                .build();
    }
}
