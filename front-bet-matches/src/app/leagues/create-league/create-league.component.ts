import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LeagueService} from "../league.service";
import {League} from "../league.model";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-create-league',
  templateUrl: './create-league.component.html',
  styleUrls: ['./create-league.component.css']
})
export class CreateLeagueComponent implements OnInit, OnDestroy {
  private loggedUserSub: Subscription;
  leagueForm: FormGroup;
  userId: string = null;


  constructor(private leagueService: LeagueService, private router: Router, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.loggedUserSub = this.loggedUserSub = this.authService.loggedUser.subscribe(user => {
        const isAuthenticated = !!user;
        if (isAuthenticated) {
          this.userId = user.id
        }
      }
    );
    this.leagueForm = new FormGroup({
      'name': new FormControl(null, [Validators.required]),
      'description': new FormControl(),
    });
  }

  ngOnDestroy() {
    this.loggedUserSub.unsubscribe();
  }

  onSubmit() {
    const leagueToAdd: League = {
      name: this.leagueForm.get('name').value,
      description: this.leagueForm.get('description').value
    };

    this.leagueService.addLeague(leagueToAdd).subscribe(
      league => {
        const leagueId = league.id;
        const nextRoot = '/leagues/' + leagueId;

        this.leagueService.addUserToLeague(leagueId, this.userId)
          .subscribe(() => this.router.navigate([nextRoot]));
      }
    );

  }
}
