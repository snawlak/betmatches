package com.bet.matches.api.core.matchprediction.client.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "matches.prediction.api")
class MatchesPredictionApiWeb {

    private String url;
}
