import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({providedIn: 'root'})
export class UserService {
  private url = environment.betMatchesUrl + '/users';

  constructor(private http: HttpClient) {
  }


}
