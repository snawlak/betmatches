import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Team} from '../teams/team.model';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Player} from './player.model';

@Injectable({providedIn: 'root'})
export class PlayerService {
  private url = environment.betMatchesUrl + '/players';

  constructor(private http: HttpClient) {
  }

  fetchPlayers() {
    return this.http
      .get<Player[]>(
        this.url
      ).pipe(
        catchError(error => {
          return throwError(error);
        })
      );
  }

}
