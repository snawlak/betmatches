package com.bet.matches.api.core.match;

import com.bet.matches.api.core.MatchDto;
import com.bet.matches.api.core.common.event.OnEndOfMatchEvent;
import com.bet.matches.api.core.common.event.OnEndOfTournamentEvent;
import com.bet.matches.api.core.footballleague.FootballLeagueService;
import com.bet.matches.api.core.matchprediction.MatchPrediction;
import com.bet.matches.api.core.matchprediction.MatchPredictionService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class MatchService {

    private final MatchRepository matchRepository;
    private final MatchValidator matchValidator;
    private final ModelMapper modelMapper;
    private final MatchPredictionService matchPredictionService;
    private final FootballLeagueService footballLeagueService;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Value("${bet.matches.football-league.spain.id}")
    private Integer footballLeagueId;

    public List<MatchDto> getAllMatches() {
        return matchRepository.getAllMatches().stream()
                .map(match -> modelMapper.map(match, MatchDto.class))
                .collect(Collectors.toList());
    }

    public List<MatchDto> getAllMatchesFromDate(final OffsetDateTime matchDate) {
        final OffsetDateTime startDate = matchDate.withHour(0).withMinute(0);
        final OffsetDateTime endDate = startDate.plusDays(1);
        return matchRepository.getAllMatchesFromDate(startDate, endDate).stream()
                .map(match -> modelMapper.map(match, MatchDto.class))
                .collect(Collectors.toList());
    }

    public List<MatchDto> getAllMatchesDtoFromMatchDay(final int matchDay) {
        return matchRepository.getAllMatchesFromMatchDay(matchDay).stream()
                .map(match -> modelMapper.map(match, MatchDto.class))
                .collect(Collectors.toList());
    }

    public List<Match> getAllMatchesFromMatchDay(final int matchDay) {
        return matchRepository.getAllMatchesFromMatchDay(matchDay);
    }

    public MatchDto getMatch(final int id) {
        return matchRepository.getMatch(id)
                .map(match -> modelMapper.map(match, MatchDto.class))
                .orElseThrow(IllegalStateException::new);
    }

    public MatchDto addMatch(final MatchDto matchDto) {
        if (matchValidator.isValid(matchDto)) {
            Match match = modelMapper.map(matchDto, Match.class);
            match = matchRepository.addOrUpdateMatch(match);

            final MatchPrediction matchPrediction = matchPredictionService.addMatchPrediction(match);

            match.setMatchPrediction(matchPrediction);

            return modelMapper.map(match, MatchDto.class);
        }
        throw new IllegalStateException("Validation of Match went wrong");
    }

    @Transactional
    public MatchDto updateMatch(final int id, final MatchDto newMatchDto) {
        if (matchValidator.isValid(newMatchDto)) {
            final Match match = matchRepository.getMatch(id)
                    .orElseThrow(RuntimeException::new);
            final MatchStatus previousStatus = match.getStatus();
            final MatchStatus newStatus = newMatchDto.getStatus();

            match.setTeamFirst(newMatchDto.getTeamFirst());
            match.setTeamSecond(newMatchDto.getTeamSecond());
            match.setTeamFirstScore(newMatchDto.getTeamFirstScore());
            match.setTeamSecondScore(newMatchDto.getTeamSecondScore());
            match.setStadium(newMatchDto.getStadium());
            match.setMatchDate(newMatchDto.getMatchDate());
            match.setStatus(newStatus);
            match.setMatchDay(newMatchDto.getMatchDay());
            match.setIdRapidApi(newMatchDto.getIdRapidApi());

            if (isMatchFinished(previousStatus, newStatus)) {
                applicationEventPublisher.publishEvent(new OnEndOfMatchEvent(this, match));
            }

            if (isMatchDayFinished(match)) {
                footballLeagueService.setNextMatchDay(footballLeagueId);
            }

            if (isLastMatchDay(match)) {
                applicationEventPublisher.publishEvent(new OnEndOfTournamentEvent(this));
            }

            return modelMapper.map(match, MatchDto.class);
        }

        throw new IllegalStateException("Validation of Match went wrong");
    }

    public boolean deleteMatch(final int id) {
        if (matchRepository.getMatch(id).isPresent()) {
            matchRepository.deleteMatch(id);
            return true;
        }
        return false;
    }

    private static boolean isMatchFinished(final MatchStatus previousStatus, final MatchStatus newStatus) {
        return (MatchStatus.FINISHED == newStatus) && (previousStatus != newStatus);
    }

    private boolean isLastMatchDay(final Match match) {
        final int matchDays = footballLeagueService.getMatchDays(footballLeagueId);
        if (match.getMatchDay() == matchDays) {
            final Optional<Match> lastMatch = matchRepository.getAllMatches().stream()
                    .filter(theMatch -> matchFromMatchDay(theMatch, matchDays))
                    .filter(theMatch -> MatchStatus.FINISHED != theMatch.getStatus())
                    .findAny();
            return lastMatch.isEmpty();
        }
        return false;
    }

    private boolean isMatchDayFinished(final Match match) {
        if (match.getMatchDay() == footballLeagueService.getCurrentMatchDay(footballLeagueId)) {
            final Optional<Match> matchesFromMatchDay = matchRepository.getAllMatches().stream()
                    .filter(theMatch -> matchFromMatchDay(theMatch, match.getMatchDay()))
                    .filter(this::matchNotFinishedAndPostponed)
                    .findAny();
            return matchesFromMatchDay.isEmpty();
        }
        return false;
    }

    private boolean matchFromMatchDay(final Match theMatch, final int matchDay) {
        return theMatch.getMatchDay() == matchDay;
    }

    private boolean matchNotFinishedAndPostponed(final Match match) {
        final MatchStatus matchStatus = match.getStatus();
        return MatchStatus.FINISHED != matchStatus
                && MatchStatus.POSTPONED != matchStatus;
    }
}
