import {Component, OnInit} from '@angular/core';
import {Team} from './team.model';
import {TeamService} from './team.service';
import {Subscription, throwError} from "rxjs";
import {PlayerService} from "../players/player.service";
import {Country} from "../common/country.service";
import {AuthService} from "../auth/auth.service";
import {Player} from "../players/player.model";

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {
  private loggedUserSub: Subscription;
  teams: Team[] = [];
  players: Player[] = [];
  selectedTeamPlayers: Player[] = [];
  teamSelected: Team;
  userId: string = null;
  isFetching = false;
  isTeamSelected = false;
  error = null;
  playersPosition = ['GOALKEEPER', 'DEFENDER', 'MIDFIELDER', 'ATTACKER'];

  constructor(private teamService: TeamService,
              private playerService: PlayerService,
              private country: Country,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.loggedUserSub = this.loggedUserSub = this.authService.loggedUser.subscribe(user => {
        const isAuthenticated = !!user;
        if (isAuthenticated) {
          this.userId = user.id
        }
      }
    );

    this.fetchTeams();
    this.fetchPlayers();
  }

  private fetchTeams() {
    this.isFetching = true;
    this.teamService.fetchTeams().subscribe(teams => {
      this.isFetching = false;
      this.teams = teams.sort((team1, team2) => team1.name.localeCompare(team2.name));
    }, error => {
      this.isFetching = false;
      throwError(error);
    });
  }

  private fetchPlayers() {
    this.isFetching = true;
    this.playerService.fetchPlayers().subscribe(players => {
      this.isFetching = false;
      this.players = players;
      this.onTeamSelected(0);
    }, error => {
      this.isFetching = false;
      throwError(error);
    });
  }

  onTeamSelected(i: number) {
    this.selectedTeamPlayers = [];
    this.teamSelected = this.teams[i];
    this.selectedTeamPlayers = this.players.filter(
      player => player.team.id === this.teamSelected.id
    );

    this.isTeamSelected = true;
  }

  getFlag(nationality: string) {
    const code = this.country.getCountryCode(nationality);
    return 'https://media.api-football.com/flags/' + code.toLowerCase() + '.svg';
  }

  getPlayers(position: string) {
    return this.selectedTeamPlayers.filter(player => player.position === position);
  }
}
