import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import {throwError} from "rxjs";
import {FootballLeague} from "./football-league.model";

@Injectable({providedIn: 'root'})
export class FootballLeagueService {
  private url = environment.betMatchesUrl + '/football-leagues/1';

  constructor(private http: HttpClient) {
  }

  getFootballLeague() {
    return this.http
      .get<FootballLeague>(
        this.url
      ).pipe(
        catchError(error => {
          return throwError(error);
        })
      );
  }
}
