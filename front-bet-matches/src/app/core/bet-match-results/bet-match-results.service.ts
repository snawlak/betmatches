import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BetMatchResult, BetMatchResultId} from "./bet-match-results.model";
import {throwError} from "rxjs";
import {Match} from "../../matches/match.model";
import {catchError, map} from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class BetMatchResultsService {
  private url = environment.betMatchesUrl + '/matches-bets';

  constructor(private http: HttpClient) {

  }

  sendBetMatchResult(betMatchResult: BetMatchResult, userId: string) {
    betMatchResult.betMatchResultId.user = userId;

    return this.http
      .post<BetMatchResult>(
        this.url,
        betMatchResult
      );
  }

  fetchBetMatchResults(userId: string) {
    const fullUrl = this.url + "/users/" + userId + "/matches/";
    return this.http
      .get<BetMatchResult[]>(
        fullUrl
      ).pipe(
        map((betMatchResults) => {
          return betMatchResults;
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  getUsersPointsFromMatchDay(userId: string, matchDay: number) {
    const fullUrl = this.url + "/users/" + userId + "/points/" + matchDay;
    return this.http
      .get<number>(
        fullUrl
      ).pipe(
        map((points) => {
          return points;
        }),
        catchError(err => {
          return throwError(err);
        })
      );
  }

  getBetMatchResult(userId: string, match: Match) {
    const fullUrl = this.url + "/users/" + userId + "/matches/" + match.id;
    return this.http
      .get<BetMatchResult>(
        fullUrl
      ).pipe(
        map((betMatchResult) => {
          return betMatchResult
        }),
        catchError(err => {
          return throwError(err)
        })
      );
  }
}
