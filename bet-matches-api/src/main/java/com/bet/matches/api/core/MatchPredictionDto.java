package com.bet.matches.api.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data

public class MatchPredictionDto {

    @JsonProperty("prediction_draw")
    @ApiModelProperty(example = "0.30")
    private float draw;

    @JsonProperty("prediction_team_first_win")
    @ApiModelProperty(example = "0.45")
    private float teamFirstWin;

    @JsonProperty("prediction_team_second_win")
    @ApiModelProperty(example = "0.25")
    private float teamSecondWin;

    @JsonProperty("prediction_team_first_goals")
    @ApiModelProperty(example = "2")
    private int teamFirstGoals;

    @JsonProperty("prediction_team_second_goals")
    @ApiModelProperty(example = "1")
    private int teamSecondGoals;
}
