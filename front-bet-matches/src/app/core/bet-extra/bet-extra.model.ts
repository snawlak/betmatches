import {User} from "../../users/user.model";
import {Player} from "../../players/player.model";
import {Team} from "../../teams/team.model";

export interface BetExtra {
  user?: User;
  player?: Player;
  team?: Team;
}
