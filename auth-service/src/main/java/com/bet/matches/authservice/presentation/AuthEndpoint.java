package com.bet.matches.authservice.presentation;


import com.bet.matches.authservice.core.common.betmatchesapi.User;
import com.bet.matches.authservice.core.login.LoginRequestBody;
import com.bet.matches.authservice.core.login.LoginResponseBody;
import com.bet.matches.authservice.core.login.LoginService;
import com.bet.matches.authservice.core.registration.RegistrationRequestBody;
import com.bet.matches.authservice.core.registration.RegistrationService;
import lombok.RequiredArgsConstructor;
import org.keycloak.common.VerificationException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("auth")
public class AuthEndpoint {

    private static final String LOGIN_ERROR_MSG = "Could not login user";

    private final RegistrationService registrationService;
    private final LoginService loginService;

    @PostMapping(value = "/registration", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public User registerUser(final RegistrationRequestBody registrationRequestBody) {
        return registrationService.registerUser(registrationRequestBody);
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public LoginResponseBody loginUser(final LoginRequestBody loginRequestBody) {
        try {
            return loginService.loginUser(loginRequestBody);
        } catch (final VerificationException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException(LOGIN_ERROR_MSG);
    }
}
