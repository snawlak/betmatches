package com.bet.matches.authservice.core.common.keycloak;

import lombok.experimental.UtilityClass;

import javax.ws.rs.core.Response;
import java.util.UUID;

@UtilityClass
public class KeycloakUtils {

    public static UUID getUserIdFromResponse(final Response response) {
        final String id = response.getLocation()
                .getPath()
                .replaceAll(".*/([^/]+)$", "$1");

        return UUID.fromString(id);
    }
}
