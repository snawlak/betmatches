package com.bet.matches.api.core.common.calculation;

import com.bet.matches.api.core.betextra.BetExtra;
import com.bet.matches.api.core.betextra.BetExtraRepository;
import com.bet.matches.api.core.player.Player;
import com.bet.matches.api.core.player.PlayerRepository;
import com.bet.matches.api.core.team.Team;
import com.bet.matches.api.core.team.TeamRepository;
import com.bet.matches.api.core.user.User;
import com.bet.matches.api.core.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.List;

@Component
@RequiredArgsConstructor
public class EndOfFootballLeagueCalc {

    private final Points points;
    private final PlayerRepository playerRepository;
    private final TeamRepository teamRepository;
    private final UserRepository userRepository;
    private final BetExtraRepository betExtraRepository;

    private int extraPoints;

    @PostConstruct
    private void init() {
        extraPoints = points.getExtra();
    }

    public void addPointsToUsersWhoBetTopScorer() {
        final List<Player> topScorers = playerRepository.getPlayersWithHighestScore();
        final Player topScorer;

        if (isOnlyOneTopScorer(topScorers)) {
            topScorer = topScorers.get(0);
        } else {
            topScorer = getTopScorerFromBestTeam(topScorers);
        }

        addPointsToUserWhoBetCorrectlyTopScorer(topScorer);
    }

    public void addPointsToUsersWhoBetWinningTeam() {
        final Team bestTeam = teamRepository.getBestTeam();
        betExtraRepository.getAllWhoBetWinningTeam(bestTeam)
                .forEach(this::addPoints);
    }

    private boolean isOnlyOneTopScorer(final List<Player> topScorers) {
        return topScorers.size() <= 1;
    }

    private void addPointsToUserWhoBetCorrectlyTopScorer(final Player player) {
        betExtraRepository.getAllWhoBetTopScorer(player)
                .forEach(this::addPoints);
    }

    private Player getTopScorerFromBestTeam(final List<Player> topScorers) {
        return topScorers.stream()
                .min(Comparator.comparing(player -> player.getTeam().getPlace()))
                .orElseThrow(RuntimeException::new);
    }

    private void addPoints(final BetExtra betExtra) {
        final User user = betExtra.getUser();
        final int userPoints = user.getPoints();
        user.setPoints(userPoints + extraPoints);
        userRepository.addOrUpdateUser(user);
    }
}
