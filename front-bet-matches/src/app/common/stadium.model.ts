export interface Stadium {
  id: number;
  name: string;
  country: string;
  city: string;
  capacity: number;
}
