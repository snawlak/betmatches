import {Component, OnInit} from '@angular/core';
import {PlayerService} from './player.service';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  constructor(private playerService: PlayerService) {
  }

  ngOnInit(): void {
    this.playerService.fetchPlayers().subscribe(
      response => {
        console.log(response);
      }
    );
  }

}
