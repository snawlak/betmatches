package com.bet.matches.api.infrastructure.persistence.footballleague;

import com.bet.matches.api.core.footballleague.FootballLeague;
import com.bet.matches.api.core.footballleague.FootballLeagueRepository;
import com.bet.matches.api.core.stadium.Stadium;
import com.bet.matches.api.core.stadium.StadiumRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
class DbFootballLeagueRepository implements FootballLeagueRepository {

    private final DbFootballLeagueJpaRepository repository;

    @Override
    public List<FootballLeague> getAllFootballLeagues() {
        return repository.findAll();
    }

    @Override
    public Optional<FootballLeague> getFootballLeague(final int id) {
        return repository.findById(id);
    }

    @Override
    public FootballLeague addOrUpdateFootballLeague(final FootballLeague stadium) {
        return repository.save(stadium);
    }

    @Override
    public void deleteFootballLeague(final int id) {
        repository.deleteById(id);
    }
}
