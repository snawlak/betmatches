import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatchService} from '../../matches/match.service';
import {Match} from '../../matches/match.model';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {NgForm} from '@angular/forms';
import {FootballLeagueService} from "../../core/football-league/football-league.service";
import {BetMatchResultsService} from "../../core/bet-match-results/bet-match-results.service";
import {BetMatchResult, BetMatchResultId} from "../../core/bet-match-results/bet-match-results.model";
import {Subscription, throwError} from "rxjs";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-bet-matches',
  templateUrl: './bet-matches.component.html',
  styleUrls: ['./bet-matches.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class BetMatchesComponent implements OnInit, OnDestroy {
  @ViewChild('f', {static: false}) slForm: NgForm;
  matches: Match [] = [];
  matchesFromMatchDay: Match [] = [];
  isFetching = false;
  selectedMatchDay;
  matchDays;
  matchDayPlayed: boolean;
  betMatchResults: BetMatchResult[] = [];
  userId: string = null;
  pointsInMatchDay: number [] = [];
  private loggedUserSub: Subscription;

  constructor(private matchService: MatchService, private footballLeagueService: FootballLeagueService,
              private betMatchResultsService: BetMatchResultsService, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.isFetching = true;

    this.loggedUserSub = this.loggedUserSub = this.authService.loggedUser.subscribe(user => {
        const isAuthenticated = !!user;
        if (isAuthenticated) {
          this.userId = user.id
        }
      }
    );

    this.footballLeagueService.getFootballLeague().subscribe(
      footballLeague => {
        this.selectedMatchDay = footballLeague.current_match_day.toString();
        this.matchDays = footballLeague.match_days;

        this.pointsInMatchDay = new Array(this.matchDays);
        for (let i = 0; i < this.matchDays; i++) {
          this.betMatchResultsService.getUsersPointsFromMatchDay(this.userId, i + 1).subscribe(
            points => this.pointsInMatchDay[i] = points
          );
        }
      }
    );

    this.betMatchResultsService.fetchBetMatchResults(this.userId).subscribe(betMatchResults => {
      this.betMatchResults = betMatchResults;
    });


    this.matchService.fetchAllMatches().subscribe(
      matches => {
        this.matches = matches;
        this.isFetching = false;
      }
    );
  }

  ngOnDestroy(): void {
    this.loggedUserSub.unsubscribe();
  }


  getDates(startDate: Date, numberOfDays: number) {
    const dates = new Array(numberOfDays);
    dates[0] = startDate;
    for (let i = 1; i < numberOfDays; i++) {
      const nextDate = new Date(startDate);
      nextDate.setDate(startDate.getDate() + i);
      dates[i] = nextDate;
    }
    return dates;
  }

  getMatchDays() {
    return Array(this.matchDays)
      .fill(0)
      .map((x, i) => i + 1);
  }

  getMatchesFromMatchDay(matchDay) {
    this.matchesFromMatchDay = this.matches.filter(match => match.match_day === +matchDay);
    this.matchDayPlayed = this.matchesFromMatchDay.filter(match => match.status !== 'FINISHED').length <= 0;
    return this.matchesFromMatchDay;
  }

  getBetTeamFirstScore(matchId) {
    let betMatchResult1 = this.betMatchResults
      .find(betMatchResult => betMatchResult.betMatchResultId.match === matchId);
    return betMatchResult1 !== undefined ? betMatchResult1.team_first_score : '?';
  }

  getBetTeamSecondScore(matchId) {
    let teamSecondScore = this.betMatchResults
      .find(betMatchResult => betMatchResult.betMatchResultId.match === matchId);
    return teamSecondScore !== undefined ? teamSecondScore.team_second_score : '?';
  }

  isNotStartedOrPostponed(status) {
    return status === 'NOT_STARTED' || status === 'POSTPONED'
  }

  onClear() {
    this.slForm.reset();
  }

  onSubmit(form: NgForm) {
    const betGoals = form.value;
    this.matchesFromMatchDay.forEach(match => {
      const teamFirst = match.id + '-team_first';
      const teamSecond = match.id + '-team_second';
      const betTeamFirstScore = betGoals[teamFirst];
      const betTeamSecondScore = betGoals[teamSecond];
      if (betTeamFirstScore !== "" && betTeamSecondScore !== "") {
        this.sendBetMatchResult(match, betTeamFirstScore, betTeamSecondScore);
      }
    });

    this.ngOnInit();
  }

  private sendBetMatchResult(match: Match, betTeamFirstScore, betTeamSecondScore) {
    const betMatchResultId: BetMatchResultId = {
      user: null,
      match: match.id
    };

    const betMatchResult: BetMatchResult = {
      team_first_score: betTeamFirstScore,
      team_second_score: betTeamSecondScore,
      points_achieved: 0,
      betMatchResultId: betMatchResultId
    };

    this.betMatchResultsService.sendBetMatchResult(betMatchResult, this.userId)
      .subscribe(() => {
        this.ngOnInit();
      }, error => {
        throwError(error)
      });
  }

  getBetMatchResultIcon(match: Match) {
    let betMatchResult1 = this.betMatchResults
      .find(betMatchResult => betMatchResult.betMatchResultId.match === match.id);

    if (betMatchResult1 !== undefined && betMatchResult1.points_achieved != null && match.status === "FINISHED") {
      const pointsAchieved = betMatchResult1.points_achieved;
      if (pointsAchieved === 3) {
        return "assets/images/win.svg";
      } else if (pointsAchieved === 1) {
        return "assets/images/draw.svg";
      } else {
        return "assets/images/defeat.svg";
      }
    } else {
      return "assets/images/draw.svg";
    }

  }
}
