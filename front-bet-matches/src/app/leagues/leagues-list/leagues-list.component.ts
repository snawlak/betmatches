import {Component, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {LeagueService} from "../league.service";
import {League} from "../league.model";
import {AuthService} from "../../auth/auth.service";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-leagues-list',
  templateUrl: './leagues-list.component.html',
  styleUrls: ['./leagues-list.component.css']
})
export class LeaguesListComponent implements OnInit, OnDestroy {
  leagues: League [] = [];
  selectedLeague: League;
  isFetching = false;
  private loggedUserSub: Subscription;

  constructor(private leagueService: LeagueService,
              private router: Router,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.loggedUserSub = this.authService.loggedUser.subscribe(user => {
        const isAuthenticated = !!user;
        if (isAuthenticated) {
          this.isFetching = true;
          this.getLeaguesOfUser(user.id);
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.loggedUserSub.unsubscribe()
  }

  getLeaguesOfUser(id: string) {
    this.leagueService.getAllLeaguesOfUser(id).subscribe(
      leagues => {
        this.leagues = this.leagues = leagues.sort(
          (league1, league2) => league1.name.localeCompare(league2.name));
        this.selectedLeague = this.leagues[0];
        this.router.navigate(['/leagues', this.selectedLeague.id])
        this.isFetching = false;
      }
    )
  }

  onLeagueSelected(league: League) {
    this.selectedLeague = league;
    this.router.navigate(['/leagues', this.selectedLeague.id])
  }
}
