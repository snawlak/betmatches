import {Component, OnInit} from '@angular/core';
import {AuthService} from "./auth/auth.service";
import {Router, RouterEvent} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  showHeader: boolean = false;

  constructor(private router: Router, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.router.events.subscribe(
      (routerEvent: RouterEvent) => {
        if (routerEvent.url !== undefined) {
          if (routerEvent.url === '/' || routerEvent.url.substring(0, 2) === '/#') {
            this.showHeader = false;
          } else {
            this.showHeader = true;
          }
        }
      });

    this.authService.autoLogin();
  }

}
