package com.bet.matches.api.core.footballleague;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "football_leagues")
@NoArgsConstructor
@AllArgsConstructor
public class FootballLeague {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "id_rapid_api", nullable = false)
    private int idRapidApi;

    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "flag", nullable = false)
    private String flag;

    @Column(name = "league_name", nullable = false)
    private String leagueName;

    @Column(name = "logo", nullable = false)
    private String logo;

    @Column(name = "teams_number", nullable = false)
    private int teamsNumber;

    @Column(name = "match_days", nullable = false)
    private int matchDays;

    @Column(name = "current_match_day", nullable = false)
    private int currentMatchDay;

    @Column(name = "start_date", nullable = false)
    private OffsetDateTime startDate;

    @Column(name = "end_date", nullable = false)
    private OffsetDateTime endDate;
}
