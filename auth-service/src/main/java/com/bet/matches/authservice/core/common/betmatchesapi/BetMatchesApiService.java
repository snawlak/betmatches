package com.bet.matches.authservice.core.common.betmatchesapi;

import com.bet.matches.authservice.core.registration.RegistrationRequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.UUID;

import static com.bet.matches.authservice.core.common.betmatchesapi.UserMapper.toBetMatchesApiUser;
import static reactor.core.publisher.Mono.just;

@Service
@RequiredArgsConstructor
public class BetMatchesApiService {

    private final WebClient betMatchesApiWebClient;

    public User registerUser(final RegistrationRequestBody registrationRequestBody,
                             final UUID userId) {
        final User user = toBetMatchesApiUser(registrationRequestBody, userId);

        return register(user);
    }

    public User getUser(final UUID userId, final String accessToken) {
        final String authorizationHeader = "Bearer " + accessToken;
        final String id = userId.toString();

        return betMatchesApiWebClient
                .get()
                .uri("/" + id)
                .header(HttpHeaders.AUTHORIZATION, authorizationHeader)
                .retrieve()
                .bodyToMono(User.class)
                .block();
    }

    private User register(final User user) {
        return betMatchesApiWebClient
                .post()
                .body(just(user), User.class)
                .retrieve()
                .bodyToMono(User.class)
                .block();
    }
}
