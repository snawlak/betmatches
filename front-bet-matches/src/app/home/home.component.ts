import {Component, OnInit} from '@angular/core';
import {AuthService} from "../auth/auth.service";
import {FootballLeagueService} from "../core/football-league/football-league.service";
import {FootballLeague} from "../core/football-league/football-league.model";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isFetching = false;
  footballLeague: FootballLeague = null;

  constructor(private footballLeagueService: FootballLeagueService, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.isFetching = true;
    this.footballLeagueService.getFootballLeague().subscribe(
      footballLeague => {
        this.footballLeague = footballLeague;
        this.isFetching = false;
      }
    );
  }

}
